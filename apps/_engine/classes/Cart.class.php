<?php
$__class__cart = null;

function cart(){
	global $__class__cart;
	if( $__class__cart === null )
		$__class__cart = new Cart();

	return $__class__cart;
}

class Cart{
	public $cart = array(
		'prods' => array(),
		'count' => 0,
		'total' => 0,
	);

	//	
	function __construct(){
		if( !isset($_SESSION['cart']) )
			$_SESSION['cart'] = $this->cart;
		else
			$this->cart = $_SESSION['cart'];

		return $this;
	}

	function count(){
		return $this->cart['count'];
	}

	function total(){
		return $this->cart['total'];
	}

	function prods($id=null){
		if($id != null)
			return isset($this->cart['prods'][$id]) ? $this->cart['prods'][$id] : false;
		else
			return $this->cart['prods'];
	}

	function save(){
		$_SESSION['cart'] = $this->cart;
		return $this;
	}

	function clear($id=0){
		//	Delete 1 item
		if( $id>0 ){
			if( isset($this->cart['prods'][$id]) ){
				unset($this->cart['prods'][$id]);
				$this->recount();
			}

		}

		//	Clear all cart
		else{
			$this->cart['prods'] = array();
			$this->cart['count'] = 0;
			$this->cart['total'] = 0;
			$this->save();
		}

		return $this;
	}

	function recount(){
		$count = $total = 0;
		foreach($this->cart['prods'] as $k=>$v){
			//	Delete empty rows
			if( !isset($v['count']) || $v['count'] < 1 ){
				unset($this->cart['prods'][$k]);
				continue;
			}

			//	Calculate prod total
			$this->cart['prods'][$k]['total'] = $v['count']*$v['cost'];

			//	Calculate cart count & total
			//$count += $v['count'];
			$total += $v['total'];
		}

		//	Save cart count & total
		$this->cart['count'] = count($this->cart['prods']);
		$this->cart['total'] = $total;

		$this->save();
	}

	//	Add
	function add($id, $count=1, $cost=null, $info=array()){
		if( !isset($this->cart['prods'][$id]) )
			$this->cart['prods'][$id] = array();

		if(!is_null($cost)) 
			$this->cart['prods'][$id]['cost'] = $cost;
		
		$this->cart['prods'][$id]['count'] = $count;
		$this->cart['prods'][$id]['total'] = $count * $this->cart['prods'][$id]['cost'];
		

		if(!empty($info))
			$this->cart['prods'][$id]['info'] = $info;

		$this->recount();

		return $this;
	}



}