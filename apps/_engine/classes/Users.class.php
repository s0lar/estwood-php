<?php
class Users{
	const SALT = "*L*F*C*";

	function __construct(){
	}

	static function checkAuth(){
		@session_start();
		// 	User not authorized
		if( !(isset( $_SESSION['user']['id'] ) && $_SESSION['user']['id'] > 0) ) {
			return false;
		}
		return true;

	}

	static function login($login, $pass){
//		exit( Users::genPass("welcome") );
		$u = Users::getUser($login);
		// User not found
		if( empty($u) ) return false;

		$res = Q("SELECT id, login, admin, root, dealer, updated FROM @@users WHERE login=?s AND pass=?s", array( $login, Users::genPass($pass) ))->row();
		if( empty($res) ) return false;

		@session_start();
		unset($_SESSION['user']);
		unset($res['updated']);
		$_SESSION['user'] = $res;
		return true;
	}

	function logout(){}

	function addUser($login, $pass, $admin=0){
		$u = $this->getUser($login);
		if( !empty( $u ) )
			return false;

		//	Add new user
		$res = Q("INSERT INTO @@users SET login=?s, pass=?s, updated=NOW(), admin=?i", array( $login, Users::genPass($pass), $admin ));
		$u = $this->getUser($login);
		return $u;
	}

	function editUser($id, $login, $pass='', $admin=0){
		$u = $this->getUser($login);

		if( !empty($u) && $id != $u['id'] )
			return false;

		if( $pass == '' ) Q("UPDATE @@users SET login=?s, updated=NOW(), admin=?i WHERE id=?i", array($login, $admin, $id));
		else Q("UPDATE @@users SET login=?s, updated=NOW(), pass=?s WHERE id=?i", array($login, Users::genPass($pass), $id));
		$u = $this->getUser($login);
		return $u;
	}

	function delUser(){}

	static function genPass($pass){
		return md5( $pass . Users::SALT );
	}

	static function getUser($login){
		return Q("SELECT id, login, admin, root, updated FROM @@users WHERE `login`=?s", array($login))->row();
	}

	function createTable(){}
}
