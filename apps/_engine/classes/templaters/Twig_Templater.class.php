<?php

class Twig_Templater extends Any_Templater
{
	public $twig;
	
	function init()
	{
		import::from($this->config['lib'].'Autoloader.php');
		import::unregister();
		
		Twig_Autoloader::register();
		
		$loader = new Twig_Loader_Filesystem(import::buildPath($this->config['template_dir']));
		$this->twig = new Twig_Environment($loader, array(
			'cache' => import::buildPath($this->config['compile_dir'])
		));
		
		import::from('app:views/twig/extensions/Twig_Extension_X.php');
		$this->twig->addExtension(new Twig_Extension_X());		
			
		return $this;
	}
	
	function display()
	{
		$template = $this->twig->loadTemplate($this->response->template.'.html');		
		return $template->render($this->response->body);
	}
}

?>