<?php
//	Project name
define("PR_NAME", "Estwood");

//	Emails list
define("mmm_DEV", "pavel.sidorov@gmail.com");


define("reCaptcha_pub", "");
define("reCaptcha_sec", "");

return array(
    'db' => array(
        'dsn'       => 'mysqli://glukhanko_estwud:mrYd1fja@localhost:3306/glukhanko_estwud?encoding=utf8&table_prefix=estwood__',
        'dsn_local' => 'mysqli://root:123@127.0.0.1:3306/estwood?encoding=utf8&table_prefix=estwood__',
    ),

    'views'      => array(
        'default' => 'Smarty_Templater',
        'html'    => 'Smarty_Templater',
        'json'    => 'JSON_Templater',
        'xml'     => 'XML_Templater'
    ),
    'templaters' => array(
        'Smarty_Templater' => array(
            'lib'          => 'shared:Smarty-3.1.29/libs',
            'template_dir' => 'app:views',
            'compile_dir'  => 'app:tmp/smarty_compiled',
            'plugins_dir'  => array('app:views/__plugins')
        )
    ),
);
