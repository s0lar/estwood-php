<?php
# which object can be created as a child
return array(
    'sections' => array(
        '#.*#' => array('sections' => '*',/*'catalog' => '*',*/),


        '#^/contacts/.*#' => array(
            'sections' => 0,
        ),

        '#^/catalog/.*#' => array(
            'sections' => 0,
            'cat1'     => '*',
        ),

        '#^/service/.*#' => array(
            'sections' => 0,
            'service'  => '*',
        ),

        '#^/information/.*#' => array(
            'sections' => 0,
            'blog'     => '*',
        ),

        '#^/trust_us/.*#' => array(
            'sections' => 0,
            'trust'    => '*',
        ),
    ),

    'cat1' => array(
        '#.*#' => array('cat2' => '*', 'sections' => 0),
    ),

    'cat2' => array(
        '#.*#' => array('prod' => '*', 'sections' => 0),
    ),
);
