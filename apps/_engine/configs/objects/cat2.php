<?php
return array(
	# types of fields
	'fields' => array(
		'title' 		=> 'string',
		'in_menu' 	=> 'checkbox',
	),

	# labels of fields	
	'ui' => array(
		'title' 		=> 'Название страницы',
		'in_menu' 	=> 'В меню',
	),

	# node configuration
	'node' => array(
		# use "title" field for "object_title" in nodes table
		'object_title' => 'title',
		# use user input for "name" field in nodes table
		'name' 			=> '-user'
	),

	# labels for actions
	'labels' => array(
		'add' 			=> 'Новый подраздел',
		'adding' 		=> 'Создание подраздела',
		'edit' 			=> 'Редактировать подраздел',
		'editing' 	=> 'Редактирование подраздела',
		'delete' 		=> 'Удалить подраздел'
	)
);