<?php
return array(
	# types of fields
	'fields' => array(
		'title' 	=> 'string',
		'anons1' 	=> 'text',
		'anons2' 	=> 'text',
		'anons3' 	=> 'text',
		'anons4' 	=> 'text',
		'list_title' => 'string',
		'content' 	=> 'html',
		#'content2' 	=> 'html',
		#'content3' 	=> 'html',
	),

	# labels of fields	
	'ui' => array(
		'title' 	=> 'Заголовок',
		'anons1' 	=> 'Блок 1',
		'anons2' 	=> 'Блок 2',
		'anons3' 	=> 'Блок 3',
		'anons4' 	=> 'Блок 4',
		'list_title' => 'Подпись к списку товаров',
		'content' 	=> 'Контент',
		#'content2' 	=> 'Контент2',
		#'content3' 	=> 'Контент3',
	),

	# node configuration
	'node' => array(
		# use "title" field for "object_title" in nodes table
		'object_title' => 'title',
		# use user input for "name" field in nodes table
		'name' 		=> '-user'
	),

	# 
	/*
	'subtabs' => array(
		'0' => array(
			'title' => 'Русская версия',
			'fields_include' => array('title','anons','subtitle','desc','link','pic_main','pics','best','t_web','t_band','t_photo','visible','sort'),
		),

		'1' => array(
			'title' => 'Английская версия',
			'fields_include' => array('title_en','anons_en','subtitle_en','desc_en'),
		),
	),
	*/

	# labels for actions
	'labels' => array(
		//'list' => 'Подразделы',
		'add' 		=> 'Новый раздел',
		'adding' 	=> 'Создание раздела',
		'edit' 		=> 'Редактировать раздел',
		'editing' 	=> 'Редактирование раздела',
		'delete' 	=> 'Удалить раздел'
	)
);