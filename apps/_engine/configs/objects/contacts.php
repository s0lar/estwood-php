<?php
return array(
	# types of fields
	'fields' => array(
		'title' 	=> 'string',
		'addr' 		=> 'text',
		'manager' 	=> 'string',
		'phone' 	=> 'string',
		'latitude' 	=> 'string',
		'longitude' => 'string',
		//'photo' 	=> 'gs_file',
		'sort' 		=> 'int',
	),

	# labels of fields	
	'ui' => array(
		'title' 	=> 'Город',
		'addr' 		=> 'Адрес',
		'manager' 	=> 'Менеджер',
		'phone' 	=> 'Телефон',
		'latitude' 	=> 'Широта',
		'longitude' => 'Долгота',
		//'photo' 	=> 'Фото (800x535px)',
		'sort' 		=> 'Порядок',
	),

	#	
	'input_cfg' => array(
		'photo' => array(
			'type' => 'image',
			'mask' => 'contacts',
		),
	),

	# node configuration
	'node' => array(
		# use "title" field for "object_title" in nodes table
		'object_title' => 'title',
		# use user input for "name" field in nodes table
		'name' => '-user'
	),

	# view
	'view' => array(
		'mode' 		=> 'list',
		'fields' 	=> array('title', 'sort'),
		'orderby' 	=> ' `sort`, `title` ',
		'edit_field' => 'title',
		'limit' 	=> 150		
	),

	# labels for actions
	'labels' => array(
		'list' 		=> 'Филиалы',
		'add' 		=> 'Добавить филиал',
		'adding' 	=> 'Добавление филиала',
		'edit' 		=> 'Редактировать филиал',
		'editing' 	=> 'Редактирование филиала',
		'delete' 	=> 'Удалить филиал'
	)
);