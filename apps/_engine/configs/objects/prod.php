<?php
return array(
	# types of fields
	'fields' => array(
		'title' 			=> 'string',
		'cost' 				=> 'string',
		'anons' 			=> 'string',
		'pic_1' 			=> 'gs_file',
		'prod_name' 	=> 'string',
		'prod_an' 		=> 'text',
		'prod_bc' 		=> 'string',
		'prod_bg_1' 	=> 'gs_file',
		//'prod_comm1' 	=> 'text',
		'prod_long' 	=> 'checkbox',
		'prod_pic_1' 	=> 'gs_file',
		'prod_pic_2' 	=> 'gs_file',
		'prod_table' 	=> 'text',
		'prod_comm2' 	=> 'text',
		'prod_source' => array(
							        'type'      =>'mn2',
							        'relation'  => array(
							                'table'     => 'source',       //  Название таблицы
							                'var'       => 'id',        //  Ключ для связи
							                'value'     => 'title',      //  Имя для отображения в CHECKBOX'е
							                'sort'      => 'title',      //  Сортировка для отображения
							            ),
							        ),
		'prod_source_pic' => 'gs_file',
		'prod_album' 	=> 'gs_file',
		'prod_order'  => 'text',
		'sort' 				=> 'int',
	),

	# labels of fields	
	'ui' => array(
		'title' 			=> 'Название',
		'cost' 				=> 'Цена',
		'anons' 			=> 'Анонс',
		'pic_1' 			=> 'Фото',
		'prod_name' 	=> 'Имя в карточке',
		'prod_an' 		=> 'Крупный комент в карточке',
		'prod_bc' 		=> 'Подпись в нав.цепочке',
		'prod_bg_1' 	=> 'Фон для первого слайда карточки',
		//'prod_comm1' 	=> 'Комент в карточке на слайде спеков',
		'prod_long' 	=> 'Прокрутка фото?',
		'prod_pic_1' 	=> 'Главное фото',
		'prod_pic_2' 	=> 'Фон для информации',
		'prod_table' 	=> 'Таблица параметров',
		'prod_comm2' 	=> 'Комментарий после таблицы',
		'prod_source' => 'Материалы',
		'prod_source_pic' => 'Фото',
		'prod_album' 	=> 'Фото галерея',
		'prod_order'  => 'Текст - заказать',
		'sort' 				=> 'Порядок',
	),

	#	
	'input_cfg' => array(
		'pic_1' => array(
			'type' => 'image',
			'mask' => 'prod_noresize',
		),
		'prod_bg_1' => array(
			'type' => 'image',
			'mask' => 'prod_noresize',
		),
		'prod_pic_1' => array(
			'type' => 'image',
			'mask' => 'prod_noresize',
		),
		'prod_pic_2' => array(
			'type' => 'image',
			'mask' => 'prod_noresize',
		),
		'prod_source_pic' => array(
			'type' => 'image',
			'mask' => 'prod_noresize',
		),
		'prod_album' => array(
			'type' => 'image',
			'mask' => 'prod_5',
		),
	),

	# node configuration
	'node' => array(
		# use "title" field for "object_title" in nodes table
		'object_title' => 'title',
		# use user input for "name" field in nodes table
		'name' 		=> '-user'
	),

	# 
	'subtabs' => array(
		'0' => array(
			'title' => 'Карточка в каталоге',
			'fields_include' => array('title','anons','cost','pic_1','sort'),
		),
		'1' => array(
			'title' => 'Карточка товара',
			'fields_include' => array('prod_name','prod_an','prod_bc','prod_bg_1'),
		),
		'2' => array(
			'title' => 'Крупный план',
			'fields_include' => array('prod_long','prod_pic_1'),
		),
		'3' => array(
			'title' => 'Информация',
			'fields_include' => array('prod_pic_2','prod_table','prod_comm2'),
		),
		'4' => array(
			'title' => 'Фотографии',
			'fields_include' => array('prod_album'),
		),
		'5' => array(
			'title' => 'Материалы',
			'fields_include' => array('prod_source', 'prod_source_pic'),
		),
		'6' => array(
			'title' => 'Как заказать',
			'fields_include' => array('prod_order'),
		),
	),

	# 
	'view' => array(
		'mode' 		=> 'list',
		'fields' 	=> array('title', 'cost', 'sort'),
		'orderby' 	=> ' `sort` DESC ',
		'edit_field' => 'title',
		'limit' 	=> 150		
	),


	# labels for actions
	'labels' => array(
		'list' 			=> 'Товары',
		'add' 			=> 'Новый товар',
		'adding' 		=> 'Создание товара',
		'edit' 			=> 'Редактировать товар',
		'editing' 	=> 'Редактирование товара',
		'delete' 		=> 'Удалить товар'
	)
);