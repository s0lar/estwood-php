<?php
return array(
	# types of fields
	'fields' => array(
		'title' 			=> 'string',
		'text' 				=> 'html',
		'photo' 			=> 'gs_file',
		'sort' 				=> 'int',
	),

	# labels of fields	
	'ui' => array(
		'title' 			=> 'Название',
		'text' 				=> 'Текст',
		'photo' 			=> 'Фото',
		'sort' 				=> 'Порядок',
	),

	#	
	'input_cfg' => array(
		'photo' => array(
			'type' => 'image',
			'mask' => 'service',
		),
	),

	# node configuration
	'node' => array(
		# use "title" field for "object_title" in nodes table
		'object_title' => 'title',
		# use user input for "name" field in nodes table
		'name' 		=> '-auto'
	),

	# labels for actions
	'labels' => array(
		'list' 			=> 'Услуги',
		'add' 			=> 'Новая услуга',
		'adding' 		=> 'Создание услуги',
		'edit' 			=> 'Редактировать услугу',
		'editing' 	=> 'Редактирование услуги',
		'delete' 		=> 'Удалить услугу'
	)
);