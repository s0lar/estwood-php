<?php
return array(
    # types of fields
    'fields'    => array(
        'title' => 'string',
        'date'  => 'date',
        'anons' => 'text',
        'pic_1' => 'gs_file',
//        'desc'  => 'html',
    ),

    # labels of fields
    'ui'        => array(
        'title' => 'Название',
        'date'  => 'Дата',
        'anons' => 'Анонс',
        'pic_1' => 'Фото',
//        'desc'  => 'Текст',
    ),

    #
    'input_cfg' => array(
        'pic_1' => array(
            'type' => 'image',
            'mask' => 'blog',
        ),
    ),

    # node configuration
    'node'      => array(
        # use "title" field for "object_title" in nodes table
        'object_title' => 'title',
        # use user input for "name" field in nodes table
        'name'         => '-user'
    ),

    #
    'view'      => array(
        'mode'       => 'list',
        'fields'     => array('title', 'date'),
        'orderby'    => ' `date` DESC ',
        'edit_field' => 'title',
        'limit'      => 150
    ),


    # labels for actions
    'labels'    => array(
        'list'    => 'Нам доверяют',
        'add'     => 'Новый пост',
        'adding'  => 'Создание поста',
        'edit'    => 'Редактировать пост',
        'editing' => 'Редактирование поста',
        'delete'  => 'Удалить пост'
    )
);