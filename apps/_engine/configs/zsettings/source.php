<?php
return array(
	# types of fields
	'fields' => array(
		'title' 			=> 'string',
		'anons' 			=> 'text',
		'subtitle' 		=> 'text',
		'title_bg' 		=> 'gs_file',
		'content' 		=> 'html',
		'options' 		=> 'text',
		'album' 			=> 'text',
		'sort' 				=> 'int',
	),

	# labels of fields	
	'ui' => array(
		'title' 			=> 'Название',
		'anons' 			=> 'Анонс',
		'subtitle' 		=> 'Название в окне',
		'title_bg' 		=> 'Фон',
		'content' 		=> 'Описание',
		'options' 		=> 'Доп. описание',
		'album' 			=> 'Варианты ообработки',
		'sort' 				=> 'Порядок',
	),

	#	
	'input_cfg' => array(
		'title_bg' => array(
			'type' => 'image',
			'mask' => 'source_bg',
		),
	),

	# node configuration
	'node' => array(
		# use "title" field for "object_title" in nodes table
		'object_title' => 'title',
		# use user input for "name" field in nodes table
		'name' 		=> '-auto'
	),

	'view' => array(
		'mode' 		=> 'list',
		'fields' 	=> array('title', 'sort'),
		'orderby' 	=> ' `sort` DESC ',
		'edit_field' => 'title',
		'limit' 	=> 150		
	),

	# labels for actions
	'labels' => array(
		'list' 			=> 'Материалы',
		'add' 			=> 'Новый материал',
		'adding' 		=> 'Создание материала',
		'edit' 			=> 'Редактировать материал',
		'editing' 	=> 'Редактирование материала',
		'delete' 		=> 'Удалить материал'
	)
);