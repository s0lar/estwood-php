<?php

class About_Controller extends ZObject_Controller
{
	function action_index()
	{
		$res = Q('SELECT * FROM @@about WHERE id=?i ', array($this->node['object_id']))->row();
		RS( 'page', $res );
		RS('navi', true);
		V('about');
	}
}

