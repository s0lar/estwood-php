<?php

class Blog_Controller extends Catalog_Controller
{
function action_index()
	{
		RS('cls_body', 'blog-p');


		//	List of posts
		if ($this->node['object_type'] == 'sections') {
			$arr = array();

			$res = Q('SELECT N.`path`, B.`title`, B.`anons`, B.`date`, B.`pic_1` FROM `@@blog` as B
					LEFT JOIN `@@nodes` as N ON B.`id` = N.`object_id` 
					WHERE N.`object_type`="blog" ORDER BY B.`date` DESC '
				);


			while ($r = $res->each()) {
				$r['pic_1'] = $this->getFM($r['pic_1'], 'a');
				$r['date'] 	= $this->buildDate($r['date']);
				$arr[] = $r;
			}


			RS('posts', $arr);
			V('blog-list');
		}
		//	Show post
		else {
			$post = Q('SELECT * FROM @@blog WHERE id=?i', array($this->node['object_id']))->row();

			$post['date'] = $this->buildDate($post['date']);
			RS('post', $post);

			V('blog-item');
		}
	}

	function buildDate($d){
		$d = $this->prepareDate($d);
		return $d['date'] . ' ' .$d['year'];
	}
}