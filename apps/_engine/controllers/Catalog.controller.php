<?php

class Catalog_Controller extends ZObject_Controller
{
	function action_index()
	{
		//	Catalog menu
		RS('menu', $this->getMenu());
		
		//	Products
		RS('prod', $this->getProd());

		RS('cls_body', 'catalog-p');
		V('catalog');
	}

	function getMenu(){
		$arr = array();
		
		$res = Q('SELECT `id`, `parent_id`, `object_type`, `object_title` as `title`, `path` FROM @@nodes WHERE `object_type`="cat1" OR `object_type`="cat2" ORDER BY `parent_id`, `sort` ');

		while ($r = $res->each()) {
			if( $r['object_type'] == 'cat1' ){
				$arr[ $r['id'] ] = $r;
			}
			if( $r['object_type'] == 'cat2' ){
				$arr[ $r['parent_id'] ]['sub'][] = $r;
			}
		}

		return $arr;
	}

	function getProd(){
		$arr = array();
		
		$res = Q('SELECT N.`path`, N.`name` as `id_name`, P.`title`, P.`anons`, P.`cost`, P.`pic_1` FROM `@@prod` as P 
					LEFT JOIN `@@nodes` as N ON P.`id` = N.`object_id` 
					WHERE N.`object_type`="prod" AND N.`path` LIKE ?s ORDER BY P.`sort` DESC ', array( $this->node['path'].'%' ));

		while ($r = $res->each()) {
			$r['pic_1'] = $this->getFM($r['pic_1'], 'b');
			$arr[] = $r;
		}
		
		return $arr;
	}
}