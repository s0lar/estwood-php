<?php

class Contacts_Controller extends ZObject_Controller
{


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function action_index()
	{
		$res = Q('SELECT * FROM @@sections WHERE id=?i ', array($this->node['object_id']))->row();
		RS('page', $res);
		//RS('navi', true);

		RS('cls_body', 'contact-p');
		V('contacts');
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Callback
	function action_feedback(){
		$arr 		= array('success'=>true);
		$errors = array();

		//	Check CSRF Token
		if ( !( !empty($_POST['csrf_feedback']) && !empty($_SESSION['csrf_feedback']) && $_POST['csrf_feedback'] === $_SESSION['csrf_feedback'] ) )
			$errors['csrf'] = 'Ваша сессия устарела. Обновите страницу и попробуйте еще раз';

		/*
		//	Check
		$response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".reCaptcha_sec."&response=".@$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']), true);

		if($response['success'] == false){
			$errors['captcha'] = 'Вы не прошли проверку капчи';
		}
		*/

		if( empty($_POST['name']) )
			$errors['name'] = 'Введите ваше имя';

		if( empty($_POST['phone']) && empty($_POST['email']) )
			$errors['phone'] = 'Укажите телефон для связи';


		//	Strip HTML tags for secure form
		foreach ($_POST as $k=>$v)
			$_POST[$k] = strip_tags($v);


		//	Есть ошибки - прерываем отправку
		if( !empty($errors) ){
			$arr['success'] = false;
			$arr['error'] = $errors;

			exit( json_encode( $arr ) );
		}

		//	Generate email
		$sm = SM();
		$sm->assign('date', $this->prepareDate(date("d.m.Y H:i")));
		$sm->assign('post', $_POST);
		$cont = $sm->fetch("mail/feedback.tpl");

		//	Include PHP mailer
		require_once SHARED_PATH.'/mmailer.php';
		$mm = mmailer();

		//	Sendto array
		$to = $this->getZcfg('email_feedback');
		$to = explode(",", $to['email_feedback']);
		foreach ($to as $key => $value)
			$mm->addAddress( trim($value) );

		$mm->Subject 	= "Обратная связь ".date("d.m.Y H:i");
		$mm->Body 		= $cont;


		$rrr = $mm->send();

		exit( json_encode( $arr ) );
	}
}