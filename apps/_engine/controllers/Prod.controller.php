<?php

class Prod_Controller extends Catalog_Controller
{
function action_index()
	{
		//	Catalog menu
		//RS('menu', $this->getMenu());

		$prod = Q('SELECT * FROM @@prod WHERE id=?i', array($this->node['object_id']))->row();
		$prod['prod_bg_1'] 	= $this->getFM($prod['prod_bg_1'], 'b');
		$prod['prod_pic_1'] = $this->getFM($prod['prod_pic_1'], 'b');
		$prod['prod_pic_2'] = $this->getFM($prod['prod_pic_2'], 'b');
		$prod['prod_album'] = $this->getFM($prod['prod_album']);
		$prod['prod_source_pic'] = $this->getFM($prod['prod_source_pic']);

		//	Get source
		$prod['prod_source'] = explode('|', $prod['prod_source']);
		$res = Q('SELECT * FROM @@source WHERE `id` IN (?li)', array($prod['prod_source']));
		$prod['prod_source_data'] = array();

		while ($r = $res->each()) {
			$r['title_bg'] = $this->getFM($r['title_bg'], 'b');
			$prod['prod_source_data'][] = $r;
		}

		/*
		if( isset($_GET['debug']) ){
		}
		*/

		RS('prod', $prod);

		RS('cls_body', 'product-p');
		V('prod');
	}
}