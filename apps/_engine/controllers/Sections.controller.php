<?php

class Sections_Controller extends ZObject_Controller
{
    function action_index()
    {
        $res = Q('SELECT * FROM @@sections WHERE id=?i ', array($this->node['object_id']))->row();
        RS('page', $res);
        RS('cls_body', 'text-p');
        V('sections');
    }
}

