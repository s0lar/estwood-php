<?php

class Service_Controller extends ZObject_Controller 
{
	function action_index() 
	{
		$res = Q('SELECT * FROM @@service ORDER BY `sort` ASC ');
		$arr = array();
		while ($r = $res->each()) {
			$r['photo'] = $this->getFM($r['photo'], 'b');
			$arr[] = $r;
		}
		RS( 'service', $arr );

		RS('cls_body', 'service-p');
		V('service');
	}
}

