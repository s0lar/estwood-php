<?php

class Source_Controller extends ZObject_Controller 
{
	function action_index() 
	{
		$arr = array();
		$res = Q('SELECT * FROM @@source ORDER BY sort DESC');

		while ($r = $res->each()) {
			$r['title_bg'] = $this->getFM($r['title_bg'], 'b');
			$arr[] = $r;
		}

		RS('sources', $arr);
		RS('cls_body', '');
		V('source');
	}
}

