<?php
@session_start();

class ZAny_Controller
{
    public $node;        // hold data from @@nodes about current node
    public $bc;
    public $places;
    public $type;


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function __construct($node)
    {
        //$c = new Cart();$c->clear();

        $this->node = $node;
        $url        = RQ()->url;

        //	FRONT END
        if (BACKEND === false) {
            //	SEO
            $u            = "http://".$url->host['raw'].$url->path;
            $seo          = Q(
                "SELECT `id`, `title`, `h1`, `key`, `desc`, `text`, `text2` FROM @@seo WHERE `node_id`=?i",
                array($this->node['id'])
            )->row();
            $seo['title'] = stripcslashes(@$seo['title']);
            $seo['h1']    = stripcslashes(@$seo['h1']);
            $seo['key']   = stripcslashes(@$seo['key']);
            $seo['desc']  = stripcslashes(@$seo['desc']);
            $seo['text']  = stripcslashes(@$seo['text']);
            $seo['text2'] = stripcslashes(@$seo['text2']);
            //
            RS("seo", $seo);

            RS("title", strip_tags($this->node['object_title']));
            RS("node", $this->node);

            //	CSRF
            $this->CSRF('csrf_presentation');
            $this->CSRF('csrf_feedback');

            //	bread crumbs
            RS("bc", $this->getBC());

            //	In menu
            RS("inMenu", $this->inMenu());
        } //	BACK END
        else {
            RS("bc", $this->getBC());
        }
    }



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	In_menu
    function inMenu()
    {
        $arr = array();

        $res = Q(
            'SELECT N.`path`, P.`title`, N.`id`, N.`sort` FROM `@@cat1` as P 
					LEFT JOIN `@@nodes` as N ON P.`id` = N.`object_id` 
					WHERE N.`object_type`="cat1" AND P.`in_menu`=1 ORDER BY N.sort DESC'
        );

        while ($r = $res->each()) {
            $arr[$r['id']] = $r;
        }

        $res = Q(
            'SELECT N.`path`, P.`title`, N.`id`, N.`sort` FROM `@@cat2` as P 
					LEFT JOIN `@@nodes` as N ON P.`id` = N.`object_id` 
					WHERE N.`object_type`="cat2" AND P.`in_menu`=1  ORDER BY N.sort DESC'
        );

        while ($r = $res->each()) {
            $arr[$r['id']] = $r;
        }

        //  Сортируем результаты
        usort(
            $arr,
            (function ($a, $b) {
                return $a['sort'] < $b['sort'];
            })
        );

        return $arr;
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	CSRF
    function CSRF($name)
    {
        if (!isset($_SESSION[$name])) {
            $_SESSION[$name] = md5('*SALT*'.rand());
        }

        return $_SESSION[$name];
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function getZcfg($name = '')
    {
        $arr = array();

        if ($name == '') {
            $res = Q("SELECT `name`, `value` FROM @@zcfg WHERE `is_global`=1");
        } else {
            $res = Q("SELECT `name`, `value` FROM @@zcfg WHERE `name`=?s", array($name));
        }

        while ($r = $res->each()) {
            $arr[$r['name']] = $r['value'];
        }

        return $arr;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function getBnr()
    {
        $arr = array();
        $res = Q("SELECT `id`, `title`, `link_name`, `link`, `icon` FROM @@bnr ORDER BY `sort` ");
        while ($r = $res->each()) {
            $r['icon'] = $this->getFM($r['icon'], 'a');
            $arr[]     = $r;
        }

        //__($arr);exit;
        return $arr;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Default action of any controller
     *
     * @return
     */
    function action_index()
    {
    }


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function getBC()
    {
        $url  = RQ()->url;
        $res  = array();
        $tmp  = array();
        $path = '/';
        foreach ($url->segments as $k => $v) {
            $path    .= $v.'/';
            $tmp[$k] = $path;
        }

        $sql = Qb("SELECT id, parent_id, name, path, object_title FROM @@nodes WHERE path IN (?ls) ", array($tmp));
        $res = Q($sql);

        $arr = array();
        while ($r = $res->each()) {
            $r['object_title'] = stripcslashes($r['object_title']);
            $arr[]             = $r;
        }

        return $arr;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function getFM($json, $thumb = '')
    {
        if ($json == '') {
            return $json;
        }
        $arr = json_decode(stripslashes($json), true);
        if (empty($arr)) {
            return array();
        }

        if ($thumb != '') {
            if (isset($arr['files'][0]['fs'][$thumb])) {
                return $arr['files'][0]['fs'][$thumb];
            } else {
                return false;
            }
        }

        return isset($arr['files']) ? $arr['files'] : array();
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function getMonthName($m)
    {
        $_m = array(
            '01' => 'Январь',
            '02' => 'Февраль',
            '03' => 'Март',
            '04' => 'Апрель',
            '05' => 'Май',
            '06' => 'Июнь',
            '07' => 'Июль',
            '08' => 'Август',
            '09' => 'Сентябрь',
            '10' => 'Октябрь',
            '11' => 'Ноябрь',
            '12' => 'Декабрь',
        );

        //
        return $_m[$m];
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function prepareDate($d, $type = 1)
    {
        $_m = array(
            '01' => 'Января',
            '02' => 'Февраля',
            '03' => 'Марта',
            '04' => 'Апреля',
            '05' => 'Мая',
            '06' => 'Июня',
            '07' => 'Июля',
            '08' => 'Августа',
            '09' => 'Сентября',
            '10' => 'Октября',
            '11' => 'Ноября',
            '12' => 'Декабря',
        );

        $_mm = array(
            '01' => 'января',
            '02' => 'февраля',
            '03' => 'марта',
            '04' => 'апреля',
            '05' => 'мая',
            '06' => 'июня',
            '07' => 'июля',
            '08' => 'августа',
            '09' => 'сентября',
            '10' => 'октября',
            '11' => 'ноября',
            '12' => 'декабря',
        );

        $_w = array(
            '1' => 'Пн',
            '2' => 'Вт',
            '3' => 'Ср',
            '4' => 'Чт',
            '5' => 'Пт',
            '6' => 'Сб',
            '7' => 'Вс',
        );

        $_ww = array(
            '1' => 'Понедельник',
            '2' => 'Вторник',
            '3' => 'Среда',
            '4' => 'Четверг',
            '5' => 'Пятница',
            '6' => 'Суббота',
            '7' => 'Воскресенье',
        );

        $time = strtotime($d);
        $ret  = array();


        if ($type == 2) {
            //	Format: Пятница, 30 мая 2014
            $ret = $_ww[date("N", $time)].", ".date("d", $time)." ".$_mm[date("m", $time)]." ".date("Y", $time);
        } else {
            //	Format: 1 января 9:00
            $ret['date'] = date("d", $time)." ".$_mm[date("m", $time)];
            $ret['time'] = date("H:i", $time);
            $ret['year'] = date("Y", $time);
        }

        return $ret;
    }

}

?>