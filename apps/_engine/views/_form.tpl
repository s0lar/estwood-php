﻿
<form action="/contacts/:feedback" class="js-validate ajax" method="post" rel="feedback">
	<div class="fieldset">
		<input type="text" name="name" placeholder="Представьтесь, пожалуйста" required class="input">
		<svg class="icon icon-user ">
			<use xlink:href="#icon-user"></use>
		</svg>
	</div>
	<div class="fieldset">
		<div class="fieldset__flex">
			<div class="fieldset__col">
				<input type="tel" name="phone" placeholder="телефон" required class="input js-phone">
				<svg class="icon icon-phone ">
					<use xlink:href="#icon-phone"></use>
				</svg>
			</div>
			<div class="fieldset__col">
				<input type="email" name="email" placeholder="Или e-mail" class="input js-email">
				<svg class="icon icon-mail ">
					<use xlink:href="#icon-mail"></use>
				</svg>
			</div>
		</div>
	</div>
	<div class="fieldset fieldset__button">
		<input type="hidden" name="page" value="{$node.object_title}">
		<input type="hidden" name="csrf_feedback" value="{$smarty.session.csrf_feedback|default:''}">
		<button type="submit" class="button gray">Отправить</button>
	</div>
</form>
