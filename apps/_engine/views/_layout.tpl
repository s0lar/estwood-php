<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>{if isset($seo.title) && !empty($seo.title)}{$seo.title}{else}Estwood — {$title|default:''}{/if}</title>
    {strip}
        <meta name="keywords" content="{if isset($seo.key) && !empty($seo.key)}{$seo.key}{/if}" />{/strip}
    {strip}
        <meta name="description" content="{if isset($seo.desc) && !empty($seo.desc)}{$seo.desc}{/if}" />{/strip}
    <meta content="width=device-width, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="yandex-verification" content="69f40d6491044c57"/>
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700&amp;amp;subset=cyrillic" rel="stylesheet">
    <link rel="stylesheet" href="/theme/css/main.css" type="text/css">
    <link rel="stylesheet" href="/theme/css/style.css" type="text/css">

    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/theme/images/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/theme/images/favicon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/theme/images/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/theme/images/favicon/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/theme/images/favicon/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/theme/images/favicon/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/theme/images/favicon/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/theme/images/favicon/apple-touch-icon-152x152.png">
    <link rel="icon" type="image/png" href="/theme/images/favicon/favicon-196x196.png" sizes="196x196">
    <link rel="icon" type="image/png" href="/theme/images/favicon/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/theme/images/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/theme/images/favicon/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="/theme/images/favicon/favicon-128.png" sizes="128x128">
    <meta name="application-name" content="Estwood">
    <meta name="msapplication-TileColor" content="#FFFFFF">
    <meta name="msapplication-TileImage" content="/theme/images/favicon/mstile-144x144.png">
    <meta name="msapplication-square70x70logo" content="/theme/images/favicon/mstile-70x70.png">
    <meta name="msapplication-square150x150logo" content="/theme/images/favicon/mstile-150x150.png">
    <meta name="msapplication-wide310x150logo" content="/theme/images/favicon/mstile-310x150.png">
    <meta name="msapplication-square310x310logo" content="/theme/images/favicon/mstile-310x310.png">

    {literal}
        <!-- Google Tag Manager -->
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({
                    'gtm.start':
                        new Date().getTime(), event: 'gtm.js'
                });
                var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-W5PC9QR');</script>
        <!-- End Google Tag Manager -->
    {/literal}


    {literal}
        <!-- Facebook Pixel Code -->
        <script>
            !function (f, b, e, v, n, t, s) {
                if (f.fbq) return;
                n = f.fbq = function () {
                    n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq) f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window, document, 'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1699144423683671');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
                    src="https://www.facebook.com/tr?id=1699144423683671&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->
    {/literal}
</head>

<body class="{$cls_body|default:''}">

{literal}
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W5PC9QR"
                height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
{/literal}

{literal}
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function () {
                try {
                    w.yaCounter44747344 = new Ya.Metrika({
                        id: 44747344,
                        clickmap: true,
                        trackLinks: true,
                        accurateTrackBounce: true,
                        webvisor: true,
                        trackHash: true
                    });
                } catch (e) {
                }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () {
                    n.parentNode.insertBefore(s, n);
                };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else {
                f();
            }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/44747344" style="position:absolute; left:-9999px;" alt=""/></div>
    </noscript>
    <!-- /Yandex.Metrika counter -->
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-103175202-1', 'auto');
        ga('send', 'pageview');

    </script>
    <!-- Rating@Mail.ru counter -->
    <script type="text/javascript">
        var _tmr = window._tmr || (window._tmr = []);
        _tmr.push({id: "2904088", type: "pageView", start: (new Date()).getTime()});
        (function (d, w, id) {
            if (d.getElementById(id)) return;
            var ts = d.createElement("script");
            ts.type = "text/javascript";
            ts.async = true;
            ts.id = id;
            ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
            var f = function () {
                var s = d.getElementsByTagName("script")[0];
                s.parentNode.insertBefore(ts, s);
            };
            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else {
                f();
            }
        })(document, window, "topmailru-code");
    </script>
    <noscript>
        <div>
            <img src="//top-fwz1.mail.ru/counter?id=2904088;js=na" style="border:0;position:absolute;left:-9999px;" alt=""/>
        </div>
    </noscript>
    <!-- //Rating@Mail.ru counter -->
    <script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = 'https://vk.com/rtrg?p=VK-RTRG-123452-aTdVN';</script>
{/literal}

{* SVG *}
{include file="_svg.tpl"}

<div class="preloader">
    <div class="preloader__icon"></div>
</div>
<header class="header animate">
    <div class="header__logo">
        <a href="/" class="logo"></a>
    </div>
    <div class="header__burger">
        <div class="burger js-burgerMenu">
            <div class="burger-label">Меню</div>
            <div class="open">
                <div class="lines">
                    <div class="line"></div>
                    <div class="line"></div>
                    <div class="line"></div>
                </div>
            </div>
            <div class="close">
                <div class="lines">
                    <div class="line"></div>
                    <div class="line"></div>
                </div>
            </div>
        </div>
        <div class="burger__mobile js-burgerMenu">
            <span class="burger__label">menu</span>
            <span class="burger__close">
                        <svg class="icon icon-m-close ">
                            <use xlink:href="#icon-m-close"></use>
                        </svg>
                    </span>
        </div>
    </div>
    <div class="header__contact">
        <div class="header__contact-label">Свяжитесь с&nbsp;нами:</div>
        <a href="mailto:info@estwood.ru" class="contact__elem">
            <svg class="icon icon-mail ">
                <use xlink:href="#icon-mail"></use>
            </svg>
            <div class="contact__tooltip">info@estwood.ru</div>
        </a>
        <a href="tel:79189908188" class="contact__elem">
            <svg class="icon icon-m-top-phone ">
                <use xlink:href="#icon-m-top-phone"></use>
            </svg>
            <div class="contact__tooltip">+7 918 990-81-88</div>
        </a>
    </div>
</header>
<div class="header-menu">
    <a href="/" class="header-menu__top">
        <svg class="icon icon-newlogo-big-index ">
            <use xlink:href="#icon-newlogo-big-index"></use>
        </svg>
        <svg class="icon icon-newlogo-mobile-menu ">
            <use xlink:href="#icon-newlogo-mobile-menu"></use>
        </svg>
        <p>Craft furniture <br>and more</p>
    </a>
    <div class="header-menu__main">
        <div class="header-nav">
            <div class="header-nav__item">
                <a href="/about/" class="header-nav__link">Компания</a>
            </div>
            <div class="header-nav__item">
                {if isset($inMenu) && !empty($inMenu)}
                    <a href="/catalog/" class="header-nav__link js-submenuLink" data-submenu="#catalog-menu">Каталог</a>
                {else}
                    <a href="/catalog/" class="header-nav__link">Каталог</a>
                {/if}

                <div class="header-nav__list">
                    <ul>
                        {foreach item=i from=$inMenu}
                            <li><a href="{$i.path}">{$i.title}</a></li>
                        {/foreach}
                    </ul>
                    <a href="/catalog/" class="header-nav__more">Показать все</a>
                </div>

            </div>
            <div class="header-nav__item">
                <a href="/service/" class="header-nav__link">Услуги</a>
            </div>
            <div class="header-nav__item">
                <a href="/materiali/" class="header-nav__link">Материалы</a>
            </div>
            <div class="header-nav__item">
                <a href="/information/" class="header-nav__link">Портфолио</a>
            </div>
            <div class="header-nav__item">
                <a href="/contacts/" class="header-nav__link">Контакты</a>
            </div>
        </div>
        <div class="header-list">
            <div class="header-list__item">
                <a href="/delivery_and_payment/" class="header-list__link">Оплата и доставка</a>
            </div>
            <div class="header-list__item">
                <a href="/trust_us/" class="header-list__link">Нам доверяют</a>
            </div>
        </div>
    </div>
    <div class="header-menu__bottom">
        <div class="header__links">
            <div class="header__social">
                <a href="https://www.instagram.com/__estwood__/" class="social__link">
                    <svg class="icon icon-insta ">
                        <use xlink:href="#icon-insta"></use>
                    </svg>
                </a>
                <a href="https://vk.com/estwoodru" class="social__link">
                    <svg class="icon icon-vk ">
                        <use xlink:href="#icon-vk"></use>
                    </svg>
                </a>
                <a href="https://www.facebook.com/estwood.ru" class="social__link">
                    <svg class="icon icon-fb ">
                        <use xlink:href="#icon-fb"></use>
                    </svg>
                </a>
            </div>
            <div class="header__dev">
                <a href="http://glukhanko.ru/" target="_blank" class="developer__link">
                    <span class="developer__label">Дизайн сайта: glukhanko.ru</span>
                    <svg class="icon icon-glukhanko ">
                        <use xlink:href="#icon-glukhanko"></use>
                    </svg>
                </a>
            </div>
        </div>
        <div class="header-menu__contact">
            <div class="contact__label">
                <a href="mailto:info@estwood.ru">info@estwood.ru</a>
            </div>
            <div class="contact__label">
                <a href="tel:+79189908188">+7 (918) 990-81-88</a>
            </div>
        </div>
    </div>
</div>

<div class="header-info">
    <div class="header-info__social">
        <a href="https://www.instagram.com/__estwood__/" class="header-info__link">
            <img src="/theme/images/top-instagram.svg">
        </a>
        <a href="https://wa.me/79996361108" class="header-info__link">
            <img src="/theme/images/top-whatsapp.svg">
        </a>
        <a href="https://t.me/Estwood" class="header-info__link">
            <img src="/theme/images/top-telegram.svg">
        </a>
    </div>
    <a href="tel:+79189908188" class="header-info__phone">
        <svg xmlns="http://www.w3.org/2000/svg" width="9.686" height="23.752">
            <path fill="currentColor"
                    d="M4.622 23.358l-.753-.658C1.037 19.558-.054 15.946.002 12.54c-.019-3.471.865-7.215 3.602-10.8l.79-.903C4.996.301 5.693.113 6.323 0c.781.019 1.91-.057 2.512.47.452.395.941 1.355.837 1.928s-.546 1.384-.856 2.042c-.31.659-.743 1.609-1.279 2.07-.8.762-2.182.217-3.01.555-1.194 3.34-1.119 6.595.02 9.849.79.16 2.173-.358 2.926.301.301.263.47.677.706 1.016.235.339.405.753.565 1.025.32.546.95 1.496.856 2.211-.028.64-.47 1.449-.941 1.834-.876.697-2.946.48-4.037.057z"/>
        </svg>
        <span>+7 (918) 990-81-88</span>
    </a>
</div>

{if isset($inMenu) && !empty($inMenu)}
    <div class="header-submenu" id="catalog-menu">
        <div class="header-subnav">
            {foreach item=i from=$inMenu}
                <div class="header-subnav__item">
                    <a href="{$i.path}" class="header-subnav__link">{$i.title}</a>
                </div>
            {/foreach}

        </div>
        <a href="/catalog/" class="header-submenu__more-link">
            <span>Показать все</span>
            <svg class="icon icon-right-arrow ">
                <use xlink:href="#icon-right-arrow"></use>
            </svg>
        </a>
    </div>
    <div class="filter">
        <div class="filter__head">
            <div class="filter__head-close js-filterClose">
                <svg class="icon icon-m-close ">
                    <use xlink:href="#icon-m-close"></use>
                </svg>
            </div>
            <div class="filter__head-label">Выберите категорию</div>
            <a href="/catalog/" class="filter__head-link">Показать все</a>
        </div>
        <div class="filter__body">
            <div class="filter__elem">
                <div class="filter__elem-head is-open js-filterElemToggle">
                    <div class="filter__elem-label">Мебель из массива</div>
                </div>
                <div style="display: block" class="filter__elem-body">
                    <ul class="filter-list">
                        {foreach item=i from=$inMenu}
                            <li>
                                <a href="{$i.path}" class="header-subnav__link">{$i.title}</a>
                            </li>
                        {/foreach}
                    </ul>
                </div>
            </div>
        </div>
    </div>
{/if}

{block name=filter}{/block}

<div class="main">
    {block name=content}{/block}

    {block name=footer}
        <div class="footer">
            <div class="footer__top">
                <div class="header-menu__top">
                    <svg class="icon icon-newlogo-big-index ">
                        <use xlink:href="#icon-newlogo-big-index"></use>
                    </svg>
                    <svg class="icon icon-newlogo-mobile-menu ">
                        <use xlink:href="#icon-newlogo-mobile-menu"></use>
                    </svg>
                    <p>Craft furniture
                        <br>and more</p>
                </div>
            </div>
            <div class="footer__main">
                <div class="header-nav">
                    <div class="header-nav__item">
                        <a href="/about/" class="header-nav__link">Компания</a>
                    </div>
                    <div class="header-nav__item">
                        <a href="/catalog/" class="header-nav__link">Каталог</a>
                    </div>
                    <div class="header-nav__item">
                        <a href="/service/" class="header-nav__link">Услуги</a>
                    </div>
                    <div class="header-nav__item">
                        <a href="/materiali/" class="header-nav__link">Материалы</a>
                    </div>
                    <div class="header-nav__item">
                        <a href="/information/" class="header-nav__link">Портфолио</a>
                    </div>
                    <div class="header-nav__item">
                        <a href="/contacts/" class="header-nav__link">Контакты</a>
                    </div>
                </div>
                <div class="header-list">
                    <div class="header-list__item">
                        <a href="/delivery_and_payment/" class="header-list__link">Оплата и доставка</a>
                    </div>
                    <div class="header-list__item">
                        <a href="/trust_us/" class="header-list__link">Нам доверяют</a>
                    </div>
                </div>
            </div>
            <div class="footer__bottom">
                <div class="header__links">
                    <div class="header__social">
                        <a href="http://instagram.com/__estwood__" class="social__link">
                            <svg class="icon icon-insta ">
                                <use xlink:href="#icon-insta"></use>
                            </svg>
                        </a>
                        <a href="https://m.vk.com/estwoodru" class="social__link">
                            <svg class="icon icon-vk ">
                                <use xlink:href="#icon-vk"></use>
                            </svg>
                        </a>
                        <a href="https://www.facebook.com/estwood.ru" class="social__link">
                            <svg class="icon icon-fb ">
                                <use xlink:href="#icon-fb"></use>
                            </svg>
                        </a>
                    </div>
                    <div class="header__dev">
                        <a href="http://glukhanko.ru" target="_blank" class="developer__link">
                            <span class="developer__label">Дизайн сайта: glukhanko.ru</span>
                            <svg class="icon icon-glukhanko ">
                                <use xlink:href="#icon-glukhanko"></use>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="header-menu__contact">
                    <div class="contact__label">
                        <a href="mailto:info@estwood.ru">info@estwood.ru</a>
                    </div>
                    <div class="contact__label">
                        <a href="tel:+79189908188">+7 (918) 990-81-88</a>
                    </div>
                </div>
            </div>
        </div>
    {/block}
</div>

{block name=modal}{/block}


<div id="modalFeedback" class="modal">
    <div class="modal__box">
        <div class="modal__head">
            <div class="modal__body">
                <h2>Заявка отправлена<br/>&nbsp;</h2>
                <div>
                    <p>Спасибо за ваше обращение! Наши менеджеры свяжутся с вами в ближайшее время, чтобы уточнить все детали.</p>
                    <p>&nbsp;</p>
                    <p><a href="/" class="button gray-border">НА ГЛАВНУЮ</a></p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="button-scroll-up js-buttonScrollUp"></div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://maps.google.com/maps/api/js?key=AIzaSyCmXoldus0Ts_OxCxsRT8oOrEfFnDWkGQs&sensor=false"></script>
<script src="/theme/js/vendor.js"></script>
<script src="/theme/js/main.js"></script>
<script src="/theme/js/s.js"></script>


</body>
</html>
{if isset($smarty.get.d)}{debug}{/if}