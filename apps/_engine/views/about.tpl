﻿{extends file='_layout.tpl'}

{block name=content}
<div class="nav__top">
	<ul class="nav">
		<li class="nav__item">Компания</li>
	</ul>
</div>
<div class="about">
	<div class="swiper-container js-aboutSwiper">
		<div class="swiper-wrapper">
			<div class="swiper-slide">
				<div style="background-image: url('/theme/images/tmp/about_back.jpg')" class="about-slide">
					<div class="about-slide__flex">
						<div class="about-slide__container">
							<div class="about-slide__col">
								
								{if isset($seo.h1) && !empty($seo.h1)}
									<h1 style="font-weight: 700;" class="medium__title">{$seo.h1}</h1>
								{else}
									<div class="medium__title">Производим мебель из дерева и металла. Своими руками.</div>
								{/if}

								{if isset($seo.text) && !empty($seo.text)}
									<p>{$seo.text|nl2br}</p>
								{else}
									<p>«Estwood» является компанией ритейлером и&nbsp;производителем мебели из массива дерева, корпусной мебели, предметов интерьера, освещения, изделий и мебели из металла.</p>
								{/if}

								<div class="about-advantage">
									<div class="about-advantage__elem">
										<div class="about-advantage__icon">
											<svg class="icon icon-production ">
												<use xlink:href="#icon-production"></use>
											</svg>
										</div>
										<div class="about-advantage__label">Собственное производство</div>
									</div>
									<div class="about-advantage__elem">
										<div class="about-advantage__icon">
											<svg class="icon icon-award ">
												<use xlink:href="#icon-award"></use>
											</svg>
										</div>
										<div class="about-advantage__label">Опыт в каждой сфере более 15 лет</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="swiper-slide">
				<div class="info-flex">
					<div class="info-flex__text">
						<div class="info-flex__text-container">
							<div class="medium__title">Все <br> начинается <br> с дерева</div>
							<p>Мы используем только ценные породы древесины. Наши изделия изготовлены из поваленных природой деревьев и возраст таких деревьев обычно от 30 и до 150 лет.</p>
						</div>
					</div>
					<div style="background-image: url('/theme/images/tmp/about_image.jpg')" class="info-flex__image"></div>
				</div>
			</div>
			<div class="swiper-slide">
				<div class="image-grid">
					<div class="image-grid__elem">
						<div style="background-image: url('/theme/images/tmp/about_example.jpg')" class="image-grid__back"></div>
					</div>
					<div class="image-grid__elem">
						<div style="background-image: url('/theme/images/tmp/about_example2.jpg')" class="image-grid__back"></div>
					</div>
					<div class="image-grid__elem">
						<div style="background-image: url('/theme/images/tmp/about_example3.jpg')" class="image-grid__back"></div>
					</div>
					<div class="image-grid__elem">
						<div style="background-image: url('/theme/images/tmp/about_example4.jpg')" class="image-grid__back"></div>
					</div>
					<div class="image-grid__elem">
						<div style="background-image: url('/theme/images/tmp/about_example5.jpg')" class="image-grid__back"></div>
					</div>
				</div>
			</div>
			<div class="swiper-slide">
				<div style="background-image: url('/theme/images/tmp/about-full-image.jpg')" class="image-section">
					<div class="image-section__text">
						<div class="image-section__text-container">
							<div class="medium__title">Кропотливая <br> ручная работа</div>
							<p>После спила, дерево проходит термообработку, по специально разработанному режиму технологов, в соответствии с породой древесины, её первоначальной влажности, толщины слэба и т.д</p>
						</div>
					</div>
				</div>
			</div>
			<div class="swiper-slide">
				<div class="image-grid">
					<div class="image-grid__elem">
						<div style="background-image: url('/theme/images/tmp/about_example6.jpg')" class="image-grid__back"></div>
					</div>
					<div class="image-grid__elem">
						<div style="background-image: url('/theme/images/tmp/about_example7.jpg')" class="image-grid__back"></div>
					</div>
					<div class="image-grid__elem">
						<div style="background-image: url('/theme/images/tmp/about_example8.jpg')" class="image-grid__back"></div>
					</div>
					<div class="image-grid__elem">
						<div style="background-image: url('/theme/images/tmp/about_example9.jpg')" class="image-grid__back"></div>
					</div>
					<div class="image-grid__elem">
						<div style="background-image: url('/theme/images/tmp/about_example10.jpg')" class="image-grid__back"></div>
					</div>
				</div>
			</div>
			<div class="swiper-slide">
				<div class="info-flex">
					<div class="info-flex__text">
						<div class="info-flex__text-container">
							<div class="medium__title">Творческий <br> процесс</div>
							<p>Далее слэб отправляется в цех, к творцам искусства, где путем многочисленных этапов обработки, обычный, ничем не приметный спил, превращается в щедевр мастера, радующий глаз его обладателя.</p>
						</div>
					</div>
					<div style="background-image: url('/theme/images/tmp/about_image2.jpg')" class="info-flex__image"></div>
				</div>
			</div>
			<div class="swiper-slide">
				<div class="image-grid">
					<div class="image-grid__elem">
						<div style="background-image: url('/theme/images/tmp/about_example11.jpg')" class="image-grid__back"></div>
					</div>
					<div class="image-grid__elem">
						<div style="background-image: url('/theme/images/tmp/about_example12.jpg')" class="image-grid__back"></div>
					</div>
					<div class="image-grid__elem">
						<div style="background-image: url('/theme/images/tmp/about_example13.jpg')" class="image-grid__back"></div>
					</div>
					<div class="image-grid__elem">
						<div style="background-image: url('/theme/images/tmp/about_example14.jpg')" class="image-grid__back"></div>
					</div>
					<div class="image-grid__elem">
						<div style="background-image: url('/theme/images/tmp/about_example15.jpg')" class="image-grid__back"></div>
					</div>
				</div>
			</div>
			<div class="swiper-slide">
				<div class="info-flex info-flex--style">
					<div style="background-image: url('/theme/images/tmp/about_image3.jpg')" class="info-flex__image"></div>
					<div class="info-flex__text">
						<div class="info-flex__text-container">
							<div class="medium__title">Тренды меняются, <br> качество остается</div>
							<p>Мы поддерживаем баланс между традициями и современными технологиями, чтобы сохранять аутентичность и натуральность продукции, при этом обеспечить ее идеальную ровность, экологичность и соответствие современным требованиям самых взыскательных клиентов.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="swiper-slide">
				<div class="image-grid">
					<div class="image-grid__elem">
						<div style="background-image: url('/theme/images/tmp/about_example16.jpg')" class="image-grid__back"></div>
					</div>
					<div class="image-grid__elem">
						<div style="background-image: url('/theme/images/tmp/about_example17.jpg')" class="image-grid__back"></div>
					</div>
					<div class="image-grid__elem">
						<div style="background-image: url('/theme/images/tmp/about_example18.jpg')" class="image-grid__back"></div>
					</div>
					<div class="image-grid__elem">
						<div style="background-image: url('/theme/images/tmp/about_example19.jpg')" class="image-grid__back"></div>
					</div>
					<div class="image-grid__elem">
						<div style="background-image: url('/theme/images/tmp/about_example20.jpg')" class="image-grid__back"></div>
					</div>
				</div>
			</div>
			<div class="swiper-slide swiper-no-swiping">
				<div class="screen-form">
					<div class="screen-form__inner">
						<div class="medium__title">Свяжитесь с нами</div>
						<div class="screen-form__text">Основными нашими направлениями являются индустриальный стиль <br> и лофт, однако мы с радостью беремся за реализацию <br> проектов в провансе или классике.</div>
						<div class="form">
							
							{include file='_form.tpl'}
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="swiper-pagination swiper-pagination--vertical"></div>
	</div>
</div>

{/block}