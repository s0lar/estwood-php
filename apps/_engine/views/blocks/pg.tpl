
{if isset($pg) && !empty($pg) && $pg.pages > 1}
	<ul class="shop__pagination pagination">
		<li class="pagination__prev"><a href="{$pg.path}-page/{$pg.prev}/">Предыдущая</a></li>
		
		{section name=i start=$pg.start loop=$pg.loop}
		<li {if $smarty.section.i.index == $pg.curr} class="active"{/if}>
				<a href="{$pg.path}-page/{$smarty.section.i.index}/">{$smarty.section.i.index}</a>
		</li>
		{/section}

		<li class="pagination__next"><a href="{$pg.path}-page/{$pg.next}/">Следующая</a></li>
	</ul>
{/if}

{*
<ul class="shop__pagination pagination">
<li class="pagination__prev"><a href="#">Предыдущая</a></li>
<li class="active"><a href="#">1</a></li>
<li><a href="#">2</a></li>
<li><a href="#">3</a></li>
<li><a href="#">4</a></li>
<li class="pagination__next"><a href="#">Следующая</a></li>
</ul>
*}