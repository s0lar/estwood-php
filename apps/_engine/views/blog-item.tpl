﻿{extends file='_layout.tpl'}

{block name=content}
<div class="nav__top">
	<ul class="nav">
		<li class="nav__item"><a href="/information/">Портфолио</a></li>
    <li class="nav__item">{$post.title|stripslashes}</li>
	</ul>
</div>

<div class="blog">
  <div class="blog-inner">
      <div class="blog-inner__head" style="background-image: url('/theme/images/tmp/blog_inner.jpg')">
          <div class="container">
            {if isset($seo.h1) && !empty($seo.h1)}
              <h1 class="blog-inner__title">{$seo.h1|stripslashes}</h1>
            {else}
              <h1 class="blog-inner__title">{$post.title|stripslashes}</h1>
            {/if}
            {*
              <div class="blog-inner__date">{$post.date}</div>
            *}
          </div>
      </div>
      <div class="blog-inner__body">
          <div class="container">
              <article class="article">
                  {$post.desc|stripslashes}

                  {if isset($post.prod) && !empty($post.prod)}
                  <div class="prod-grid">
                      <a href="#" class="prod-tile">
                          <div class="prod-tile__image"><img src="images/tmp/prod_tile.png" alt=""></div>
                          <div class="prod-tile__info">
                              <div class="prod-tile__info-top">
                                  <div class="prod-tile__title">СТОЛ «РИВА»</div>
                                  <div class="prod-tile__price">51 000 руб.</div>
                              </div>
                              <div class="prod-tile__info-main">
                                  Стол из слэбов горного карагача со вставкой из стекла, основание металл полированный, размеры (мм): 1600 (Ш), 900 (Д), 750 (В)
                              </div>
                              <div class="prod-tile__info-bottom">
                                  <button class="button gray-border">Подробнее</button>
                              </div>
                          </div>
                      </a>
                      <a href="#" class="prod-tile">
                          <div class="prod-tile__image"><img src="images/tmp/prod_tile.png" alt=""></div>
                          <div class="prod-tile__info">
                              <div class="prod-tile__info-top">
                                  <div class="prod-tile__title">СТОЛ «РИВА»</div>
                                  <div class="prod-tile__price">51 000 руб.</div>
                              </div>
                              <div class="prod-tile__info-main">
                                  Стол из слэбов горного карагача со вставкой из стекла, основание металл полированный, размеры (мм): 1600 (Ш), 900 (Д), 750 (В)
                              </div>
                              <div class="prod-tile__info-bottom">
                                  <button class="button gray-border">Подробнее</button>
                              </div>
                          </div>
                      </a>
                  </div>
                  {/if}

                  {$post.desc2}
                  
                  <div class="article__button">
                      <a href="/information/" class="button gray">Вернуться</a>
                  </div>
              </article>
          </div>
      </div>
  </div>
 
</div>
{/block}