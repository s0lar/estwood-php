﻿{extends file='_layout.tpl'}

{block name=filter}
    <div class="filter">
        <div class="filter__head">
            <div class="filter__head-close js-filterClose">
                <svg class="icon icon-m-close ">
                    <use xlink:href="#icon-m-close"></use>
                </svg>
            </div>
            <div class="filter__head-label">Выберите категорию</div>
            {if $request->url->path != '/catalog/'}
                <a href="/catalog/" class="filter__head-link">Показать все</a>
            {/if}

        </div>
        <div class="filter__body">
            {foreach item=i from=$menu}
                <div class="filter__elem">
                    <div class="filter__elem-head is-open js-filterElemToggle">
                        {*<div class="filter__elem-label">{$i.title}</div>*}
                        <a href="{$i.path}" class="filter__elem-label">{$i.title}</a>
                    </div>
                    <div style="display: block" class="filter__elem-body">
                        <ul class="filter-list">
                            {foreach item=ii from=$i.sub}
                                <li><a href="{$ii.path}">{$ii.title}</a></li>
                            {/foreach}
                        </ul>
                    </div>
                </div>
            {/foreach}
        </div>
    </div>
{/block}


{block name=content}
    <div class="catalog">
        <div class="catalog-burger js-catalogBurger" data-submenu="#catalog-menu">
            <div class="catalog-burger__label">
                {if $request->url->path == '/catalog/'}
                    каталог товаров
                {else}
                    {$node.object_title}
                {/if}
            </div>
            <svg class="icon icon-filter-dots ">
                <use xlink:href="#icon-filter-dots"></use>
            </svg>
        </div>
        <div class="filter-burger js-filterBurger">
            <div class="filter-burger__icon">
                <svg class="icon icon-filter-dots ">
                    <use xlink:href="#icon-filter-dots"></use>
                </svg>
            </div>
            <div class="filter-burger__label">Категории товаров</div>
        </div>
        <div class="swiper-container js-catalogSwiper">
            <div class="swiper-wrapper">
                <div class="swiper-slide">

                    {foreach item=i from=$prod name=n}

                    {if !$smarty.foreach.n.first && $smarty.foreach.n.index % 3 == 0}
                </div>
                <div class="swiper-slide">
                    {/if}

                    <a href="{$i.path}" class="catalog-elem {if $smarty.foreach.n.index % 3 == 0}catalog-elem--large{/if}"  id="{$i.id_name}">
                        <div class="catalog-elem__image">
                            <img src="{$i.pic_1}">
                        </div>
                        <div class="catalog-elem__bottom">
                            <div class="catalog-elem__title">{$i.title|stripslashes}</div>
                            <div class="catalog-elem__price">{$i.cost|stripslashes} руб.</div>
                            <div class="catalog-elem__description">{$i.anons|stripslashes}</div>
                        </div>
                    </a>

                    {/foreach}

                </div>


                <div class="swiper-slide">
                    <div class="screen-material">
                        <div class="screen-material__inner">
                            <div class="scroll-container js-scroll">
                                <div class="catalog__article">

                                    {if isset($seo.h1) && !empty($seo.h1)}
                                        <h1>{$seo.h1}</h1>
                                    {/if}

                                    {if isset($seo.text) && !empty($seo.text)}
                                        <p>{$seo.text|nl2br}</p>
                                    {/if}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="swiper-button swiper-button--fill swiper-button--prev">
                <svg class="icon icon-left-arrow ">
                    <use xlink:href="#icon-left-arrow"></use>
                </svg>
            </div>
            <div class="swiper-button swiper-button--fill swiper-button--next">
                <svg class="icon icon-right-arrow ">
                    <use xlink:href="#icon-right-arrow"></use>
                </svg>
            </div>
        </div>
        <div class="form-section visible-xs">
            <div class="screen-form__title">Свяжитесь <br> с нами</div>
            <div class="screen-form__text">
                <p>Вас заинтересовало что-то из товаров в наличии? Интересует изготовление на заказ?</p>
                <p>Отправьте нам заявку, мы свяжемся с вами и все обсудим.</p>
            </div>
            <div class="form">
                {include file='_form.tpl'}
            </div>
        </div>
    </div>
{/block}