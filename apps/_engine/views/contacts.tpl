{extends file='_layout.tpl'}

{block name=content}
    <div class="contact">
        <div class="contact__box">
            <div class="medium__title contact--title">Контактная
                <br> информация
            </div>
            <div class="contact__tabs">
                <div class="contact__tab-header">
                    <a href="#tab-1" class="contact__tab-link is-active js-contactTabLink" data-coords="45.0390935, 38.98478649999993">Г.
                        Краснодар</a>
                    <a href="#tab-2" class="contact__tab-link js-contactTabLink" data-coords="55.714282, 37.589132800000016">Г. Москва</a>
                </div>
                <div class="contact__tab-body">
                    <div id="tab-1" class="contact__items" style="display: block;">
                        <div class="contact__social">
                            <div class="header-info__social">
                                <a href="https://www.instagram.com/__estwood__/" class="header-info__link">
                                    <img src="/theme/images/top-instagram.svg">
                                </a>
                                <a href="https://t.me/Estwood" class="header-info__link">
                                    <img src="/theme/images/top-telegram.svg">
                                </a>
                                <a href="https://wa.me/79996361108" class="header-info__link">
                                    <img src="/theme/images/top-whatsapp.svg">
                                </a>
                            </div>
                        </div>
                        <div class="contact__line">
                            <div class="contact__line-icon">
                                <svg class="icon icon-mail ">
                                    <use xlink:href="#icon-mail"></use>
                                </svg>
                            </div>
                            <div class="contact__line-label">
                                <a href="mailto:info@estwood.ru">info@estwood.ru</a>
                            </div>
                        </div>
                        <div class="contact__line">
                            <div class="contact__line-icon">
                                <svg class="icon icon-phone ">
                                    <use xlink:href="#icon-phone"></use>
                                </svg>
                            </div>
                            <div class="contact__line-label">+7 (918) 990-81-88</div>
                        </div>
                        <div class="contact__line">
                            <div class="contact__line-icon">
                                <svg class="icon icon-location ">
                                    <use xlink:href="#icon-location"></use>
                                </svg>
                            </div>
                            <div class="contact__line-label">Шоурум: ул. Северная, 311/1</div>
                        </div>
                        <div class="contact__line">
                            <div class="contact__line-icon">
                                <img src="/theme/images/clock-contact.svg" width="18"/>
                            </div>
                            <div class="contact__line-label">ПН — ПТ: с 9:00 до 18:00<br>СБ — ВС: Выходные</div>
                        </div>
                    </div>
                    <div id="tab-2" class="contact__items">
                        <div class="contact__line">
                            <div class="contact__line-icon">
                                <svg class="icon icon-mail ">
                                    <use xlink:href="#icon-mail"></use>
                                </svg>
                            </div>
                            <div class="contact__line-label">
                                <a href="mailto:info@estwood.ru">info@estwood.ru</a>
                            </div>
                        </div>
                        <div class="contact__line">
                            <div class="contact__line-icon">
                                <svg class="icon icon-phone ">
                                    <use xlink:href="#icon-phone"></use>
                                </svg>
                            </div>
                            <div class="contact__line-label">+7 (999) 636-11-08</div>
                        </div>
                        <div class="contact__line">
                            <div class="contact__line-icon">
                                <svg class="icon icon-location ">
                                    <use xlink:href="#icon-location"></use>
                                </svg>
                            </div>
                            <div class="contact__line-label">Пункт выдачи: Ленинский проспект, 26</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contact__bottom">
                <button class="button gray-border js-contactModalLink">Отправить заявку</button>
            </div>
        </div>
        <div class="contact__map">
            <div id="map" data-pin="/theme/images/pin.png" data-loc="45.0390935, 38.98478649999993" class="js-map"></div>
        </div>
        <div class="contact__modal js-contactModal">
            <div class="modal__close js-contactModalClose">
                <svg class="icon icon-m-close ">
                    <use xlink:href="#icon-m-close"></use>
                </svg>
            </div>
            <div id="send" class="screen-form">
                <div class="screen-form__inner">
                    <div class="screen-form__title">Свяжитесь с нами</div>
                    <div class="screen-form__text">
                        Основными нашими направлениями являются индустриальный стиль
                        <br> и лофт, однако мы с радостью беремся за реализацию
                        <br> проектов в провансе или классике.
                    </div>
                    <div class="form">
                        {include file='_form.tpl'}
                    </div>
                </div>
            </div>
        </div>
    </div>
{/block}