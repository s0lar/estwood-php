<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru">
    <head>
    	<title>Страница не найдена. Ошибка 404 / Page not found. Error 404</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        {literal}
        <style type="text/css">
        	*{border:0;padding:0;margin:0}
        	body{ font:100% Tahoma; color:#777 }
        	h2, p, hr {margin-bottom: 10px}
        	h2{ font-weight: normal }
        	hr { height:1px; color: #333; background: #333 }
        </style>
        {/literal}
	</head>
	<body>
		<div style="margin-left:100px;width:700px;padding-top:100px">
			
			<h2><a href="/">{$smarty.const.PR_NAME}</a>{* - страница не найдена*}</h2>
			<p>&nbsp;</p>
			
			<h2>Страница не найдена.</h2>
			<p>Ошибка 404</p>
			<p>&nbsp;</p>
			<p>Вернуться <a href="/">на главную страницу</a></p>
			
			<hr />

			<h2>Page not found.</h2>
			<p>Error 404</p>
			<p>&nbsp;</p>
			<p>Back to <a href="/">main page</a></p>


		</div>
	</body>
</html>