{extends file='_layout.tpl'}

{block name=content}
    <div class="main__video">
        <video id="video-main" width="100%" height="auto" muted="muted" preload="auto">
            <source src="/theme/video/background-imovie.mp4">
        </video>
    </div>
    <div style="background-image: url('/theme/images/back/main_back.jpg')" class="main__back"></div>
    <div class="main-info">
        <div class="main__slogan animate">
            <svg class="icon icon-newlogo-big-index ">
                <use xlink:href="#icon-newlogo-big-index"></use>
            </svg>
            <h1 class="main__slogan-title">
                Производство мебели <br>и предметов интерьера
            </h1>
        </div>
        <div class="main__cat animate">
            <a href="/about/" class="cat-control">
                <div class="cat-control__icon">
                    <svg class="icon icon-production ">
                        <use xlink:href="#icon-production"></use>
                    </svg>
                </div>
                <div class="cat-control__title hidden-xs">КОМПАНИЯ</div>
                <div class="cat-control__title visible-xs">О КОМПАНИИ</div>
            </a>
            <a href="/catalog/" class="cat-control">
                <div class="cat-control__icon">
                    <svg class="icon icon-pricetag ">
                        <use xlink:href="#icon-pricetag"></use>
                    </svg>
                </div>
                <div class="cat-control__title">КАТАЛОГ</div>
            </a>
            <a href="/service/" class="cat-control hidden-xs">
                <div class="cat-control__icon">
                    <svg class="icon icon-service ">
                        <use xlink:href="#icon-service"></use>
                    </svg>
                </div>
                <div class="cat-control__title">УСЛУГИ</div>
            </a>
            <a href="tel:+79189908188" class="cat-control size_middle">
                <div class="cat-control__icon">
                    <img src="/theme/images/tel.svg" width="10" alt="">
                </div>
                <div class="cat-control__title">+7 (918) 990-81-88</div>
            </a>

            <a href="/contacts/" class="cat-control size_small hidden-xs">
                <div class="cat-control__icon">
                    <img src="/theme/images/clock.svg" width="22" alt="">
                </div>
                <div class="cat-control__title">ВРЕМЯ РАБОТЫ</div>
            </a>
            <a href="/contacts/" class="cat-control size_middle hidden-xs">
                <div class="cat-control__icon">
                    <img src="/theme/images/mail.svg" width="24" alt="">
                </div>
                <div class="cat-control__title">ОТПРАВИТЬ ЗАЯВКУ</div>
            </a>
        </div>
        <div class="main__button visible-xs">
            <a href="/contacts/#send" class="button white-border">Свяжитесь с нами</a>
        </div>
        <div class="main-address">
            <span class="main-address__city hidden-xs">шоурум: г. Краснодар, ул. Северная, 311/1</span>
            <br class="hidden-xs">
            <span class="main-address__time">ПН — ПТ: с 9:00 до 18:00</span><span class="main-address__time">СБ — ВС: Выходные</span>
        </div>
        <div class="main__links">
            <div class="main__social">
                <a href="http://instagram.com/__estwood__" class="social__link">
                    <svg class="icon icon-insta ">
                        <use xlink:href="#icon-insta"></use>
                    </svg>
                </a>
                <a href="https://m.vk.com/estwoodru" class="social__link">
                    <svg class="icon icon-vk ">
                        <use xlink:href="#icon-vk"></use>
                    </svg>
                </a>
                <a href="https://www.facebook.com/estwood.ru" class="social__link">
                    <svg class="icon icon-fb ">
                        <use xlink:href="#icon-fb"></use>
                    </svg>
                </a>
            </div>
            <a href="http://www.glukhanko.ru/" class="developer__link">
                <span class="developer__label">Дизайн сайта: glukhanko.ru</span>
                <svg class="icon icon-glukhanko ">
                    <use xlink:href="#icon-glukhanko"></use>
                </svg>
            </a>
        </div>

    </div>
{/block}