﻿{extends file='_layout.tpl'}

{block name=filter}

<div class="filter">
	<div class="filter__head">
		<div class="filter__head-close js-filterClose">
			<svg class="icon icon-m-close ">
				<use xlink:href="#icon-m-close"></use>
			</svg>
		</div>
		<div class="filter__head-label">Выберите категорию</div>
		<a href="#" class="filter__head-link">Показать все</a>
	</div>
	<div class="filter__body">
		{foreach item=i from=$menu}
		<div class="filter__elem">
			<div class="filter__elem-head is-open js-filterElemToggle">
				<div class="filter__elem-label">{$i.title}</div>
			</div>
			<div style="display: block" class="filter__elem-body">
				<ul class="filter-list">
					{foreach item=ii from=$i.sub}
					<li><a href="{$ii.path}">{$ii.title}</a></li>

					{/foreach}
				</ul>
			</div>
		</div>

		{/foreach}
	</div>
</div>

{/block}


{block name=content}

<div class="fullpage__nav">
	<div class="nav__top">
		<ul class="nav">
			<li class="nav__item">
				<a href="javascript:;" data-slide-index="2" class="nav__link">Крупный план</a>
			</li>
			<li class="nav__item">
				<a href="javascript:;" data-slide-index="3" class="nav__link">Информация</a>
			</li>
			<li class="nav__item">
				<a href="javascript:;" data-slide-index="4" class="nav__link">Фотографии</a>
			</li>
			<li class="nav__item">
				<a href="javascript:;" data-slide-index="5" class="nav__link">Материалы</a>
			</li>
			<li class="nav__item">
				<a href="javascript:;" data-slide-index="6" class="nav__link">Как заказать</a>
			</li>
		</ul>
	</div>
	<div class="nav__bottom">
		<a href="/catalog/?id={$node.name}" class="nav__return">
			<span>Назад в каталог</span>
			<svg xmlns="http://www.w3.org/2000/svg" width="23" height="13" viewBox="0 0 23 13">
				<path d="M19.8 1.7H7.2V0L0 2.4l7.2 2.3V3.3h12.6c.3 0 1.7 0 1.7 1.6v4.9c0 .4-.1.7-.3 1-.4.5-1.1.6-1.4.6h-17V13h17.1c.3 0 1.5-.1 2.3-1 .5-.6.8-1.3.8-2.2V4.9c0-1.6-1.9-3.2-3.2-3.2z" fill="currentColor"/>
			</svg>
		</a>
	</div>
</div>
<div class="swiper-container fullpage js-fullpageSwiper">
	<div class="swiper-wrapper">

		<div class="swiper-slide">
			<div class="first-screen">
				<div style="background-image: url('{$prod.prod_bg_1}')" class="first-screen__back"></div>
				<div class="first-screen__info">
					<div class="first-screen__title">{$prod.prod_name|stripslashes}</div>
					<div class="first-screen__subtitle">
						{$prod.prod_an|stripslashes|nl2br}
					</div>
				</div>
				<div class="screen__arrow js-arrowDown">
					<svg class="icon icon-m-scroll-down ">
						<use xlink:href="#icon-m-scroll-down"></use>
					</svg>
				</div>
			</div>
		</div>

	{if $prod.prod_long == 1}
		<div class="swiper-slide swiper-no-swiping">
			<div class="screen-table">
				<div class="screen-table__image js-rangeImage">
					<img src="{$prod.prod_pic_1}">
				</div>
				<div class="range-slider">
					<input type="hidden" class="js-rangeSlider">
				</div>
			</div>
		</div>
	{else}
		<div class="swiper-slide">
			<div class="screen-table">
				<div class="screen-table__image-single image--tower">
					<img src="{$prod.prod_pic_1}">
				</div>
			</div>
		</div>
	{/if}

		<div class="swiper-slide">
			<div style="background-image: url('{$prod.prod_pic_2}')" class="screen-info">
				<div class="screen-info__inner">
					<div class="screen-info__head">
						<div class="screen-info__title">{$prod.prod_name|stripslashes}</div>
						<div class="screen-info__subtitle">
							{$prod.prod_an|stripslashes}
						</div>
						<div class="screen-info__price hidden-xs">{$prod.cost} руб.</div>
					</div>
					<div class="screen-info__body">
						<div class="info-char__grid">
							{$prod.prod_table|stripslashes}
						</div>
						<div class="visible-xs">
							<div class="screen-info__bottom">
								<div class="screen-info__price">{$prod.cost} руб.</div>
								<a href="javascript:;" data-scroll-link="#orderForm" class="button gray-border">Как заказать</a>
							</div>
						</div>
						<div class="info-char__description">
							{$prod.prod_comm2|stripslashes}
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="swiper-slide">
			<div class="image-grid">
			{foreach item=i from=$prod.prod_album}
				<div class="image-grid__elem">
					<div style="background-image: url('{$i.fs.b}')" class="image-grid__back"></div>
				</div>

			{/foreach}
			</div>
		</div>


		<div class="swiper-slide">
			<div class="screen-material">
				<div class="screen-material__inner">
					<div class="screen-material__col">
						<div class="screen-material__tab">
							<div class="tab-nav">
							{foreach item=i from=$prod.prod_source_data name=n}
								<div data-tab-nav="#tab_{$smarty.foreach.n.iteration}" class="tab-nav__item {if $smarty.foreach.n.first}is-active{/if}">{$i.title|stripslashes}</div>
							{/foreach}
							</div>
							<div class="tab-content">
							{foreach item=i from=$prod.prod_source_data name=n}
								<div id="tab_{$smarty.foreach.n.iteration}">
									<div class="medium__subtitle">
										{$i.anons|stripslashes}
									</div>

									<div class="screen-material__button">
										<a href="javascript:;" data-modal="#modalVariant_{$smarty.foreach.n.iteration}" class="button gray-border js-modalLink">Подробнее</a>
									</div>
								</div>

							{/foreach}
							</div>
						</div>
					</div>

					{foreach item=i from=$prod.prod_source_pic name=n}
						<div data-tab-material="#tab_{$smarty.foreach.n.iteration}" class="screen-material__image {if $smarty.foreach.n.first}is-show{/if}">
							<img src="{$i.fs.b}">
						</div>

					{/foreach}
				</div>
			</div>
		</div>


		<div id="orderForm" class="swiper-slide">
			<div style="background-image: url('{$prod.prod_pic_2}')" class="screen-form">
				<div class="screen-form__inner">
					<div class="screen-form__title">
						Как <br> заказать
					</div>
					<div class="screen-form__text">
						{$prod.prod_order|stripslashes|nl2br}
					</div>
					<div class="form">
						{include file='_form.tpl'}
					</div>
				</div>
			</div>
		</div>


		{if !empty($seo.h1) or !empty($seo.text)}
		<div class="swiper-slide">
			<div class="screen-material">
				<div class="screen-material__inner">
					<div class="scroll-container js-scroll">
						<div class="catalog__article">

							{if isset($seo.h1) && !empty($seo.h1)}
								<h1>{$seo.h1}</h1>
							{/if}

							{if isset($seo.text) && !empty($seo.text)}
								<p>{$seo.text|nl2br}</p>
							{/if}
							
						</div>
					</div>
				</div>
			</div>
		</div>
		{/if}

	</div>
</div>

{/block}


{block name=modal}


{foreach item=i from=$prod.prod_source_data name=n}

<div id="modalVariant_{$smarty.foreach.n.iteration}" class="modal">
	<div class="modal__box">
		<div class="modal__head">
			<div class="modal__head-title">
				<div class="medium__title">
					{$i.subtitle|stripslashes|nl2br}
				</div>
			</div>
			<div style="background-image: url('{$i.title_bg}')" class="modal__image"></div>
		</div>
		<div class="modal__body">
			{$i.content|stripslashes}
			{$i.options|stripslashes}
		</div>
		<div class="modal__footer">
			{$i.album}
		</div>
	</div>
</div>

{/foreach}

{/block}