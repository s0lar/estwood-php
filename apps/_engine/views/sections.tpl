{extends file='_layout.tpl'}

{block name=content}
    <div class="text">
        <div class="text-inner">
            <div class="text-inner__head">
                <div class="container">
                    {if isset($seo.h1) && !empty($seo.h1)}
                        <h1 class="blog-inner__title">{$seo.h1|stripslashes}</h1>
                    {else}
                        <h1 class="blog-inner__title">{$page.title|stripslashes}</h1>
                    {/if}
                </div>
            </div>
            <div class="text-inner__body">
                <div class="container">
                    <article class="article">
                        {$page.content|stripslashes}
                    </article>
                </div>
            </div>
        </div>
    </div>
{/block}