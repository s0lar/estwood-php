﻿{extends file='_layout.tpl'}

{block name=filter}
{/block}


{block name=content}

<div class="nav__top">
	<ul class="nav">
		<li class="nav__item">
			<h1 style="font-size: 18px;">
			{if isset($seo.h1) && !empty($seo.h1)}
				{$seo.h1}
			{else}
				Услуги
			{/if}
			</h1>
			
		</li>
	</ul>
</div>
<div class="service">
	<div class="swiper-container js-serviceSwiper">
		<div class="swiper-wrapper">

		{foreach item=i from=$service}
			<div class="swiper-slide">
				<div class="service-elem">
					<div style="background-image: url('{$i.photo}')" class="service-elem__bg"></div>
					<div class="service-elem__info">
						<div class="service-elem__title">{$i.title|stripslashes}</div>
						<div class="service-elem__description">
							<div class="service-elem__description-inner">
								{$i.text|stripslashes}
							</div>
						</div>
					</div>
				</div>
			</div>
		{/foreach}


		</div>
		<div class="swiper-pagination"></div>
		<div class="swiper-button swiper-button--prev">
			<svg class="icon icon-left-arrow ">
				<use xlink:href="#icon-left-arrow"></use>
			</svg>
		</div>
		<div class="swiper-button swiper-button--next">
			<svg class="icon icon-right-arrow ">
				<use xlink:href="#icon-right-arrow"></use>
			</svg>
		</div>
	</div>
</div>

{/block}

{block name=footer}{/block}