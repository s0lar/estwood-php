﻿{extends file='_layout.tpl'}

{block name=content}
    <div class="nav__top">
        <ul class="nav">
            {if isset($seo.h1) && !empty($seo.h1)}
                <li class="nav__item"><h1 style="font-size: 18px;">{$seo.h1}</h1></li>
            {else}
                <li class="nav__item">Материалы</li>
            {/if}
        </ul>
    </div>
    {* Slides *}
    <div class="service material_swiper">
        <div class="swiper-container js-serviceSwiper">
            <div class="swiper-wrapper">

                {foreach item=i from=$sources}
                    <div class="swiper-slide">
                        <a href="javascript:;" data-modal="#modalDecor_{$i.id}" class="service-elem js-modalLink">
                            <div style="background-image: url('{$i.title_bg}')" class="service-elem__bg"></div>
                            <div class="service-elem__info">
                                <div class="service-elem__title">{$i.title}</div>
                                <div class="service-elem__description">
                                    <div class="service-elem__description-inner">
                                        <p>{$i.anons}</p>
                                    </div>
                                    <div class="service-elem__more-link">
                                        <span>Подробнее</span>
                                        <img src="/theme/images/nav-arrow.svg" alt="">
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                {/foreach}

            </div>

            <div class="swiper-pagination"></div>
            <div class="swiper-button swiper-button--prev">
                <svg class="icon icon-left-arrow ">
                    <use xlink:href="#icon-left-arrow"></use>
                </svg>
            </div>
            <div class="swiper-button swiper-button--next">
                <svg class="icon icon-right-arrow ">
                    <use xlink:href="#icon-right-arrow"></use>
                </svg>
            </div>
        </div>
    </div>
    {* Modals *}

    {foreach item=i from=$sources}
        <div id="modalDecor_{$i.id}" class="modal">
            <div class="modal__box">
                <div class="modal__head">
                    <div class="modal__head-title">
                        <div class="medium__title">{$i.title}</div>
                    </div>
                    <div style="background-image: url('{$i.title_bg}')" class="modal__image"></div>
                </div>
                <div class="modal__body">
                    {$i.content|stripslashes}
                    {$i.options|stripslashes}
                </div>
                <div class="modal__footer">
                    {$i.album}
                </div>
            </div>
        </div>
    {/foreach}

{/block}