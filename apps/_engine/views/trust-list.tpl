﻿{extends file='_layout.tpl'}

{block name=content}
    <div class="nav__top">
        <ul class="nav">
            {if isset($seo.h1) && !empty($seo.h1)}
                <li class="nav__item"><h1 style="font-size: 18px;">{$seo.h1}</h1></li>
            {else}
                <li class="nav__item">Нам доверяют</li>
            {/if}
        </ul>
    </div>
    <div class="blog">
        <div class="swiper-container js-blogSwiper">
            <div class="swiper-wrapper">
                {foreach item=i from=$posts}
                    <div class="swiper-slide">
                        <div class="blog-elem__inner">
                            <div class="blog-elem__date">{$i.date}</div>
                            <div class="blog-elem__image" style="background-image: url('{$i.pic_1}')"></div>
                            <div class="blog-elem__title">{$i.title}</div>
                            <div class="blog-elem__description">{$i.anons}</div>
                        </div>
                    </div>
                {/foreach}
            </div>
            <div class="swiper-pagination"></div>
            <div class="swiper-button swiper-button--prev style_gray style_sm">
                <svg class="icon icon-left-arrow ">
                    <use xlink:href="#icon-left-arrow"></use>
                </svg>
            </div>
            <div class="swiper-button swiper-button--next style_gray style_sm">
                <svg class="icon icon-right-arrow ">
                    <use xlink:href="#icon-right-arrow"></use>
                </svg>
            </div>
        </div>
        <div class="screen-form visible-xs">
            <div class="screen-form__inner">
                <div class="medium__title">Свяжитесь с&nbsp;нами</div>
                <div class="screen-form__text">Основными нашими направлениями являются индустриальный стиль
                    <br> и лофт, однако мы с радостью беремся за реализацию
                    <br> проектов в провансе или классике.
                </div>
                <div class="form">
                    {include file='_form.tpl'}
                </div>
            </div>
        </div>
    </div>
{/block}