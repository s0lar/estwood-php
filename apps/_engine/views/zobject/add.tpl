{include file='zobject/header.tpl'}	

	<ul class="nav nav-tabs" style="margin-bottom:0">
		<li class="active"><a href="{$request->url->path}:edit">{$object.labels.adding}</a></li>
	</ul>
	
	<div class="" style="padding:20px;border:1px solid #ddd;margin-top:-1px">
		
		{include file='zobject/blocks/add_objects.tpl' inline='true'}
		
	</div>

{include file='zobject/footer.tpl'}