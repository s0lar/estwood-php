﻿{if count($object.subobjects)}
	<fieldset class="group">
		<legend class="group-title">Создать</legend>
		
		{foreach item=i from=$object.subobjects}
			<a href="{$request->url|action:'add'|view:''}-type/{$i.type}/" class="add-object">{$i.labels.add}</a>		
		{/foreach}
	</fieldset>
	
	
	{*
	<br />
	<fieldset class="group">
		<legend class="group-title">Настройки</legend>
		
		{foreach item=i from=$object.subobjects}
			<a href="{$request->url|action:'cfg'|view:''}-type/{$i.type}/" class="add-object">{$i.labels.cfg}</a>
		{/foreach}
	</fieldset>
	*}
{/if}