
	<ul class="nav nav-tabs" style="margin-bottom:0">
		<li {if $request->url->action == 'edit'}class="active"{/if}><a href="{$request->url->path}:edit">{$object.labels.editing}</a></li>
		
		{if isset($object.subobjects) && !empty($object.subobjects)}
			{foreach item=i from=$object.subobjects}
				{if isset( $i.labels.actions ) && !empty( $i.labels.actions )}
					{foreach item=ii from=$i.labels.actions}
						<li {if $request->url->action == $ii.act}class="active"{/if}>
							<a href="{$request->url->path}:{$ii.act}/-type/{$i.type}"><span class="icon-th-list"></span> {$ii.name}</a>
						</li>
						
					{/foreach}
				{/if}
			{/foreach}
		{/if}
		
		{*<li {if $request->url->action == 'edit_node'}class="active"{/if}><a href="{$request->url->path}:edit_node">Настройки раздела</a></li>*}
		<li {if $request->url->action == 'edit_node_sort'}class="active"{/if}><a href="{$request->url->path}:edit_node">Редактировать порядок</a></li>
		<li {if $request->url->action == 'edit_system'}class="active"{/if}><a href="{$request->url->path}:edit_system">Настройки раздела</a></li>
		
		<li {if $request->url->action == 'seo_edit'}class="active"{/if}><a href="{$request->url->path}:seo_edit">СЕО - настройки</a></li>
	</ul>
