{literal}
	<!-- TinyMCE -->
	<script language="javascript" type="text/javascript" src="/backend/tiny_mce3/tiny_mce.js"></script>
	<script language="javascript" type="text/javascript">

	tinyMCE.init({
	        mode : "textareas",
	        theme : "advanced",
	        editor_selector : "visual",
			plugins : "flash,style,simplebrowser,table,save,advimage,advlink,iespell,preview,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,gentable",
	        //plugin_simplebrowser_browselinkurl : '/res/shared/tiny_mce3/plugins/simplebrowser/browser.html?Connector=connectors/php/connector.php',
	        //plugin_simplebrowser_browseimageurl : '/res/shared/tiny_mce3/plugins/simplebrowser/browser.html?Type=Image&Connector=connectors/php/connector.php',
	        //plugin_simplebrowser_browseflashurl : '/res/shared/tiny_mce3/plugins/simplebrowser/browser.html?Type=Flash&Connector=connectors/php/connector.php',
			plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,images",
			//plugins : "filemanager,safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
			theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontsizeselect,help",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,images,cleanup,code,|,forecolor,backcolor",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,fullscreen",
			theme_advanced_buttons4 : "template",
			theme_advanced_toolbar_location : "top",
	        theme_advanced_toolbar_align : "left",
	        theme_advanced_path_location : "bottom",
	        content_css : "/theme/css/tinymce.css",
	        plugin_insertdate_dateFormat : "%Y-%m-%d",
	        plugin_insertdate_timeFormat : "%H:%M:%S",
	        //extended_valid_elements : "hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]",
	        //	Allow all tags
	        valid_elements : '*[*]',
	        file_browser_callback : "fileBrowserCallBack",
	        theme_advanced_resize_horizontal : true,
	        theme_advanced_resizing : true,
	        convert_newlines_to_brs: false,
	        force_br_newlines: true,
	        force_p_newlines: false,
			forced_root_block : "", 
	        language : "ru",
			relative_urls : false,
			convert_urls : false,
			width : '100%',
			height : '300px',
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//	templates
			template_templates : [
					{
						title 		: "Комплекты. Описание",
						src 		: "/backend/tiny_mce3/plugins/template/tpl__complect_table.htm",
						description : "--"
					}
	        ],
	        
			entity_encoding : "raw"
	        });
	</script>
	<!-- /TinyMCE -->
{/literal}