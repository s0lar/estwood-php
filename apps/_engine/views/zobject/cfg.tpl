{include file='zobject/header.tpl'}	
	<fieldset class="group">
		<legend class="group-title">Настройка</legend>
		
		{if isset($error) && $error == '1146'}
			<p>Таблица <b>@@{$object.table}</b> не создана.</p>
			<table class='table' style='width:50%'>
				<tr>
					<th>Название</th>
					<th>Тип</th>
				</tr>
				<tr>
					<td>ID</td>
					<td>превичный ключ</td>
				</tr>
				
			{foreach item=item from=$object.fields key=key}
				<tr>
					<td>{$key}</td>
					<td>
						{if is_array($item) && isset($item.type)}
							{$item.type}
						{else}
							{$item}
						{/if}
						
					</td>
				</tr>	
					
				{/foreach}
			</table>
			{*<p><a href="../:create_table">Создать</a></p>*}
			<p><a href="{$request->url->path}:createtable/-type/{$smarty.get.type}/">Создать таблицу</a></p>
		
		{else}
			<p>Таблица <b>@@{$object.table}</b> создана.</p>

		{/if}
		
	</fieldset>
{include file='zobject/footer.tpl'}
