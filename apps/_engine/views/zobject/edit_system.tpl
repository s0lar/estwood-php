{include file='zobject/header.tpl'}

{include file='zobject/blocks/list_controll.tpl' inline='true'}

<div class="" style="padding:20px;border:1px solid #ccc;margin-top:-1px">
	<form method="POST">
		<h4>Изменить системное имя</h4>
		<div style="margin-bottom:15px" class="row-fluid border-bottom1 mb10">
			<label style="min-height:20px" for="system_name" class="span3 struct_label">Системное имя</label>
			<div class='span6'>
				<input type="text" class="input-text" name='system_name' value='{$system_name}' id='system_name' style='width:90%'><br />
				<small>Латинские символы, цифры и символы _-</small>
			</div>
		</div>
		<div class="well well-panel well-panel-controll">
			<input type="submit" class="btn btn-primary1" value="Применить">
		</div>
	</form>

	<p>&nbsp;</p>

	<form method="POST" action="{$request->url->path}:movenode">
		<h4>Переместить раздел <br />{$node.object_title}</h4>
		<div style="margin-bottom:15px" class="row-fluid border-bottom1 mb10">
			<label style="min-height:20px" for="system_name" class="span3 struct_label">В раздел: </label>
			<div class='span6'>
				<div class="nodes-padding">
					<ul id="nodes_user" class="nodes_user">
						<li><div class="nodes-item-holder">
							<a href="/" class="folder icon-minus-sign" rel="1"></a><a id="nodes_user-tree-node-item-1" href="/:edit/">Root</a>
							<input type="checkbox" class="checkbox" name="user_nodes[1]" {if isset($u.ac.nodes.nodes[1])} checked="checked" {/if} />
						</div></li>
					</ul>
				</div>

				<script type="text/javascript"> var nodes_user_json = {$u.ac|@json_encode|default:''}; </script>
			</div>
		</div>
		<div class="well well-panel well-panel-controll">
			<input type="submit" class="btn btn-primary1" value="Применить">
		</div>
	</form>
</div>


{include file='zobject/footer.tpl'}