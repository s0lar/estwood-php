{include file='zobject/header.tpl'}

	{include file='zobject/blocks/list_controll.tpl' inline='true'}
	
	<div class="" style="padding:20px;border:1px solid #ddd;margin-top:-1px">
		
		<div class="well well-panel">
			{foreach item=i from=$object.subobjects}<a href="{$request->url->path}:add?type={$smarty.get.type}" class="btn btn-mini btn-info"><span class="icon-plus-sign icon-white"></span> {$i.labels.add}</a> &nbsp;&nbsp;&nbsp;{/foreach}
			
		</div>
		
		<table class="table table-striped table-bordered">
			<tr>
				{foreach item=i from=$config->view.fields name=loop}				
					<th{if $smarty.foreach.loop.last} class="last"{/if}>{$config->ui[$i]}</th>
				{/foreach}
				
				<th class="span2"></th>
			</tr>
			{foreach item=i from=$items}
				<tr>
					{foreach item=j from=$i key=k}
						{if 'path' neq $k && 'id' neq $k}
							<td>
								{if $k eq $config->view.edit_field}
									<a href="{$i.path}:edit/">{$j|stripslashes}</a>
								{else}
									{$j|stripslashes}
								{/if}
							</td>
						{/if}
					{/foreach}
					
					
					<td>
						<a href="{$i.path}:edit/" class="btn btn-small"><span class="icon-edit"></span></a>
						<a href="{$i.path}:delete/" class="btn btn-small btn-danger" onclick="return confirm('Удалить?')"><span class="icon-remove icon-white"></span></a>
					</td>
					
				</tr>
			{/foreach}
		</table>
	</div>
	
{include file='zobject/footer.tpl'}
