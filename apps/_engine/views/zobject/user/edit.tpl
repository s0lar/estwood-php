{include file='zobject/header.tpl'}	

	<fieldset class="group">
		<legend class="group-title">Редактирование пользователя</legend>
		<form action="{$request->url->path}:user_edit.html" method="post">
				<p class="field">
					<label class="field-title" for="login">Логин (латинские символы)</label>
					<input type="text" name="login" id="login" class="input-text" style="width:300px" value="{$u.login}" />
					{if isset($smarty.get.err) && $smarty.get.err == 'empty_login'}&nbsp;&nbsp;&nbsp;<span style="color:red">Пустой логин</span>
					{elseif isset($smarty.get.err) && $smarty.get.err == 'login_exist'}&nbsp;&nbsp;&nbsp;<span style="color:red">Логин <b>{$smarty.session.wrong_login}</b> - занят</span>
					{/if}
					<br />Например: <b>manager</b> или <b>user@mail.ru</b>
				</p>
				
				<p style="color:green"><br /><b>Если не хотите менять пароль, то не вводите "Новый пароль"</b><br />&nbsp;</p>
				
				<p class="field">
					<label class="field-title" for="pass">Новый пароль</label>
					<input type="text" name="pass" id="pass" class="input-text" style="width:300px" />
					{if isset($smarty.get.err) && $smarty.get.err == 'pass2'}&nbsp;&nbsp;&nbsp;<span style="color:red">Новый пароль введен не верно!</span>
					{/if}
				</p>
				
				<p class="field">
					<label class="field-title" for="pass2">Повторите новый пароль</label>
					<input type="text" name="pass2" id="pass2" class="input-text" style="width:300px" /><br />
				</p>
				
				{include file="zobject/user/access_rules.tpl"}
				
				{*
				<p class="field">
					<label class="field-title">
						<input type="checkbox" name="admin" {if $u.admin==1}checked="checked"{/if} />
						Доступ в панель управления сайтом
					</label>
				</p>
				*}
				
				{*<p class="field">
					<label class="field-title">
						<input type="checkbox" name="dealer" {if $u.dealer==1}checked="checked"{/if} />
						Доступ ко всем дилерам
					</label>
				</p>*}
				
				
				
				<div class="btn-group">
					<input type="hidden" name="id" value="{$u.id}" />
					<input type="submit" value="Сохранить" class="btn btn-primary" />
					<input type="submit" name="save" value="Применить" class="btn btn-primary1" />
				</div>
				<a href="/:user_list" class="btn btn-small pull-right">Отмена</a>
			
			
		</form>
	</fieldset>
{include file='zobject/footer.tpl'}
