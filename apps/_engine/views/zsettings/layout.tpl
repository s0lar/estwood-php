<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>{if isset($seo.title) && !empty($seo.title)}{$seo.title}{else}{$title|default:''}{/if} — IVA Group</title>
	{strip}<meta name="keywords" content="{if isset($seo.key) && !empty($seo.key)}{$seo.key}{/if}" />{/strip}
	{strip}<meta name="description" content="{if isset($seo.desc) && !empty($seo.desc)}{$seo.desc}{/if}" />{/strip}
	<meta name="viewport" content="width=1000">

	<link href="/theme/css/style.min.css" rel="stylesheet" media="screen">
	<link rel="stylesheet" href="/theme/bower_components/slick.js/slick/slick.css" />
	<link rel="stylesheet" href="/theme/bower_components/slick.js/slick/slick-theme.css" />

	<link rel="apple-touch-icon-precomposed" sizes="57x57"    href="/theme/favicon/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114"  href="/theme/favicon/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72"    href="/theme/favicon/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144"  href="/theme/favicon/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon-precomposed" sizes="60x60"    href="/theme/favicon/apple-touch-icon-60x60.png" />
	<link rel="apple-touch-icon-precomposed" sizes="120x120"  href="/theme/favicon/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon-precomposed" sizes="76x76"    href="/theme/favicon/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon-precomposed" sizes="152x152"  href="/theme/favicon/apple-touch-icon-152x152.png" />
	<link rel="icon" type="image/png" href="/theme/favicon/favicon-196x196.png" sizes="196x196" />
	<link rel="icon" type="image/png" href="/theme/favicon/favicon-96x96.png" sizes="96x96" />
	<link rel="icon" type="image/png" href="/theme/favicon/favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="/theme/favicon/favicon-16x16.png" sizes="16x16" />
	<link rel="icon" type="image/png" href="/theme/favicon/favicon-128.png" sizes="128x128" />
	<meta name="application-name" content="IVA Group — торговое оборудование"/>
	<meta name="msapplication-TileColor" content="#FFFFFF" />
	<meta name="msapplication-TileImage" content="favicon/mstile-144x144.png" />
	<meta name="msapplication-square70x70logo" content="favicon/mstile-70x70.png" />
	<meta name="msapplication-square150x150logo" content="favicon/mstile-150x150.png" />
	<meta name="msapplication-wide310x150logo" content="favicon/mstile-310x150.png" />
	<meta name="msapplication-square310x310logo" content="favicon/mstile-310x310.png" />
</head>

<body>

	<div class="page-wrapper">

		<header class="page-header {$page_header_class|default:''}">

			<div class="page-header__top-line">
				<div class="container">
					<div class="row">
						<div class="col-xs-3">
							<a href="mailto:info@iva-group.ru"><i class="icon-mail-blue"></i><span>info@iva-group.ru</span></a>
						</div>
						<div class="col-xs-4">
							{* cart
							<a href="shop-cart.html" class="page-header__basket-link"><i class="icon-basket-blue"></i><span>Корзина: 14 750 руб.</span></a>
							*}
						</div>
						<div class="col-xs-5">
							<div class="contact-phones">
								<span class="contact-phones__l"><i class="icon-phone1-blue"></i>8 (918) 277-00-07</span>
								<span class="contact-phones__r"><i class="icon-phone2-blue"></i>8 (861) 200-05-18</span>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="page-header__main-line  container">
				<div class="row">
					<div class="page-logo  col-xs-3"><a href="/">IVA Группа компаний</a></div>
					<nav class="main-nav  col-xs-9">
						<ul class="main-nav__list">
							<li{if strstr($request->url->raw, '/info/')} class="main-nav__active"{/if}><a href="/info/">Информация</a></li>
							<li{if strstr($request->url->raw, '/services/')} class="main-nav__active"{/if}><a href="/services/">Услуги</a></li>
							<li{if strstr($request->url->raw, '/shop/')} class="main-nav__active"{/if}><a href="/shop/">Магазин</a></li>
							<li{if strstr($request->url->raw, '/portfolio/')} class="main-nav__active"{/if}><a href="/portfolio/">Портфолио</a></li>
							<li{if strstr($request->url->raw, '/contacts/')} class="main-nav__active"{/if}><a href="/contacts/">Контакты</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</header>

		<div class="page-content">
			{block name='body'}{/block}
		</div>

		<footer class="page-footer">

			<div class="page-footer__top">
				<div class="container">
					<div class="row">
						<div class="col-xs-3">
							<div class="footer-block">
								<h3 class="footer-block__caption">Информация</h3>
								<ul>
									<li><a href="/info/">О группе компаний</a></li>
									<li><a href="/info/#cat4">Оборудование</a></li>
									<li><a href="/portfolio/">Портфолио</a></li>
								</ul>
							</div>
						</div>
						<div class="col-xs-9">
							<div class="footer-block">
								<h3 class="footer-block__caption">Услуги</h3>
								<div class="row">
									<div class="col-xs-4">
										<ul>
											<li><a href="/services/razrabotka_dizain-koncepcii_torgovih_obektov/">Разработка дизайн‑концепции торговых&nbsp;объектов</a></li>
											<li><a href="/services/dizain-proektirovanie/">Дизайн-проектирование</a></li>
										</ul>
									</div>
									<div class="col-xs-4">
										<ul>
											<li><a href="/services/izgotovlenie_torgovogo_oborydovaniya/">Торговая мебель на заказ</a></li>
											<li><a href="/services/montaj_torgovogo_oborydovaniya/">Монтаж оборудования</a></li>
											<li><a href="/services/osveshenie_torgovih_obektov/">Освещение объектов</a></li>
										</ul>
									</div>
									<div class="col-xs-4">
										<ul>
											<li><a href="/articles/potolochnie_sistemi_grilyato/">Потолочные системы</a></li>
											<li><a href="/services/proizvodstvo_vistavochnogo_oborydovaniya/">Выставочное оборудование</a></li>
											<li><a href="/services/garantiinoe_i_servisnoe_obslyjivanie/">Сервисное обслуживание</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="page-footer__mid">
				<div class="container">
					<div class="row">
						<div class="col-xs-3">
							<a href="mailto:info@iva-group.ru"><i class="icon-mail-blue"></i><span>info@iva-group.ru</span></a>
						</div>
						<div class="col-xs-4">
							{* cart
							<a href="shop-cart.html" class="page-header__basket-link"><i class="icon-basket-blue"></i><span>Корзина: 14 750 руб.</span></a>
							*}
						</div>
						<div class="col-xs-5">
							<div class="contact-phones">
								<span class="contact-phones__l"><i class="icon-phone1-blue"></i>8 (918) 277-00-07</span>
								<span class="contact-phones__r"><i class="icon-phone2-blue"></i>8 (861) 200-05-18</span>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="page-footer__bot">
				<div class="container">
					<div class="row">
						<div class="col-xs-3">
							<a href="" class="logo-white">IVA Группа компаний</a>
						</div>
						<div class="col-xs-6  page-footer__bot-text">
							г. Краснодар, ул. Сормовская, д. 12 Е <br>
							<small><a class="td-alt" target="_blank" href="http://maps.yandex.ru/-/CVCdbZyJ">Яндекс.карты</a>, <a class="td-alt" target="_blank" href="https://www.google.ru/maps/place/%D0%A1%D0%BE%D1%80%D0%BC%D0%BE%D0%B2%D1%81%D0%BA%D0%B0%D1%8F+%D1%83%D0%BB.,+12%D0%B5,+%D0%9A%D1%80%D0%B0%D1%81%D0%BD%D0%BE%D0%B4%D0%B0%D1%80,+%D0%9A%D1%80%D0%B0%D1%81%D0%BD%D0%BE%D0%B4%D0%B0%D1%80%D1%81%D0%BA%D0%B8%D0%B9+%D0%BA%D1%80%D0%B0%D0%B9,+350018/@45.0243116,39.0670985,18z/data=!4m7!1m4!3m3!1s0x40f05a98b907440f:0xb5811d0707c47b86!2z0KHQvtGA0LzQvtCy0YHQutCw0Y8g0YPQuy4sIDEy0LUsINCa0YDQsNGB0L3QvtC00LDRgCwg0JrRgNCw0YHQvdC-0LTQsNGA0YHQutC40Lkg0LrRgNCw0LksIDM1MDAxOA!3b1!3m1!1s0x40f05a98b907440f:0xb5811d0707c47b86">Google maps</a></small>
						</div>
						<div class="col-xs-3  page-footer__bot-text">
							<a class="glukhanko" href="http://www.Glukhanko.ru">
								Дизайн сайта —
								<span class="glukhanko__link">www.Glukhanko.ru</span>
							</a>
						</div>
					</div>
				</div>
			</div>

		</footer>

		{block name='modal'}{/block}

	</div>


{*
	<script src="/theme/bower_components/jquery/dist/jquery.js"></script>
	<script src="/theme/bower_components/slick.js/slick/slick.min.js"></script>
	<script src="/theme/js/build/plugins.js"></script>
	<script src="/theme/js/build/script.js"></script>
	<script src="/theme/js/s.js"></script>
*}
	<script src="/min/g=js"></script>

</body>
</html>