<?php

// Caching ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function cache_get($var, $cache_time=0, $unserialize=0){
	if($cache_time)
		$res = Q("SELECT * FROM @@cache WHERE var=?s AND datetime>?s", array( $var, date("Y-m-d H:i:s", time()-$cache_time) ));
	else 
		$res = Q("SELECT * FROM @@cache WHERE var=?s", array( $var ));
	
	if( $res->numRows() == 0 ) return false;
	if( $unserialize ){
		$r = $res->row();
		$arr = unserialize($r['value']);
		return $arr;
	}
	else{
		$arr = $res->row();
		return $arr['value'];
	}
}
//	
function cache_add($var, $value, $all_pages=0){
	cache_build();
	Q("INSERT INTO @@cache (`var`, `value`, `datetime`, all_pages) VALUES (?s, ?s, NOW(), ?i) ON DUPLICATE KEY UPDATE `value`=VALUES(`value`), `datetime`=NOW(), `all_pages`=VALUES(`all_pages`)", array($var, $value, $all_pages));
}
//	
function cache_build(){
	Q("CREATE TABLE IF NOT EXISTS @@cache ( 
		    `id` int(11) unsigned NOT NULL auto_increment,
			  `var` varchar(255) default NULL,
			  `value` longtext,
			  `datetime` timestamp NULL default NULL,
			  `all_pages` int(11) default '0',
			  PRIMARY KEY  (`id`),
			  UNIQUE KEY `var` (`var`),
			  KEY `all_pages` (`all_pages`)
			) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;");
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//	Get Smarty 
function SM(){
    $config = import::config('app:configs/app.php')->templaters['Smarty_Templater'];
    import::from($config['lib'].'/Smarty.class.php');
    $smarty = new Smarty(); 
    $smarty->template_dir = import::buildPath($config['template_dir']);
    $smarty->compile_dir = import::buildPath($config['compile_dir']);
    return $smarty;
}

//	Clear GET params
function _get($s, $type=''){
	switch ($type) {
		case 'int':
			$result = isset( $_GET[$s] ) ? intval( $_GET[$s] ) : false;
			break;
		
		case 'floatval':
			$result = isset( $_GET[$s] ) ? floatval( $_GET[$s] ) : false;
			break;

		default:
			$result = isset( $_GET[$s] ) ? filter_var( $_GET[$s], FILTER_SANITIZE_STRING ) : false;
			break;
	}
	return $result;
}

//	Clear POST params
function _post($s, $type=''){
	switch ($type) {
		case 'int':
			$result = isset( $_POST[$s] ) ? intval( $_POST[$s] ) : false;
			break;
		
		case 'floatval':
			$result = isset( $_POST[$s] ) ? floatval( $_POST[$s] ) : false;
			break;

		default:
			$result = isset( $_POST[$s] ) ? filter_var( $_POST[$s], FILTER_SANITIZE_STRING ) : false;
			break;
	}
	return $result;
}

//	Clear XSS params
function xss_clear($s){
    return filter_var( $s, FILTER_SANITIZE_STRING );
}

//	Translitterate
function translit( $text ) {
	$text = mb_strtolower($text, 'utf-8');
	
	$trans = array(
		"а"=>"a","б"=>"b","в"=>"v","г"=>"g",
		"д"=>"d","е"=>"e", "ё"=>"yo","ж"=>"j",
		"з"=>"z","и"=>"i","й"=>"i","к"=>"k",
		"л"=>"l", "м"=>"m","н"=>"n","о"=>"o",
		"п"=>"p","р"=>"r","с"=>"s","т"=>"t", 
		"у"=>"y","ф"=>"f","х"=>"h","ц"=>"c",
		"ч"=>"ch", "ш"=>"sh","щ"=>"sh","ы"=>"i",
		"э"=>"e","ю"=>"u","я"=>"ya",
		
		" " => "_",
		";" => "",
		"/" => "",
		"\\" => "",
		//"." => "",
		"," => "",
		"=" => "",
		"@" => "",
		"#" => "",
		"\$" => "",
		"%" => "",
		"^" => "",
		"&" => "",
		"*" => "",
		"(" => "",
		")" => "",
		"`" => "",
		"\"" => "",
		"'" => "",
		"+" => "",
		"«" => "",
		"»" => "",
		
		"ь"=>"","Ь"=>"","ъ"=>"","Ъ"=>"");
	
	$translit = strtr($text, $trans);
    
    return $translit;
}

//	Redirect
function redirect($s=''){
	if( $s=='' ) $s = $_SERVER['REQUEST_URI'];
	header("Location: ".$s); exit;
}

//	Redirect with JavaScript timeout
function redirect2($s=''){
	if( !isset( $_SESSION['redirect2'] ) ){
		@session_start();
		$_SESSION['redirect2'] = 0;
	}
	
	if( $_SESSION['redirect2'] > 5 ) {
		$_SESSION['redirect2']++;
		redirect($s);
	}
	else
	{
		$_SESSION['redirect2'] = 0;
		echo "
		<html>
		<head>
			<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
		</head>
		<body>
			<p>" .( isset( $_GET['msg'] ) ? urldecode($_GET['msg']) : ('Подождите, идет перенаправление.') ). "!</p>
			
			<script type='text/javascript'>
				setTimeout(redirect(), 5000);
				function redirect(){
					location.href='".$s."'
				}
			</script>
		</body>
		</html>
		";
		exit;
	}
}


/*
 * PG - Paginator
 * 
 * $sql 	- SQL query to get all items count
 * $limit 	- limit of selected items 
 * 
 * @return array()
 */

function PG($sql, $limit=10){
 	// 	Init array
	$arr = array(
		'start' 	=> 1,
		'loop' 		=> 0,
		'limit' 	=> $limit,
		'curr' 		=> isset($_GET['page']) ? intval($_GET['page']) : 1,
		'offset' 	=> 0,
		'count' 	=> 0,
		'pages' 	=> 0,
	);
	
	// 	Init offset
	$arr['offset'] = $limit * ($arr['curr']-1);
	
	// 	Get items count
	$res = Q( $sql );
	$arr['count'] = $res->numRows();
	
	// 	Calculate pages count
	$arr['pages'] = ceil($arr['count'] / $limit);
	
	// 	Loops, for Smarty section
	$arr['loop'] = $arr['pages']+1;
	
	// 	Unset 'page' param from GET array to prepare pager query string
	unset($_GET['page']);
	
	// 	Get query string
	$url = RQ()->url;
	$action = '';
	if( $url->action != 'index' )
		$action = ':'.$url->action.'/';
		
	$path = $url->path;
	$arr['path'] = $path . $action . get2path( $_GET );
	
	if( $arr['pages'] > 1 ){
		$arr['left_arr'] = 1;
		$arr['right_arr'] = 1;

		//	Prev page
		if($arr['curr']-1 > 0 ) $arr['prev'] = $arr['curr']-1;
		else $arr['prev'] = 1;

		//	Next page
		if($arr['curr']+1 <= $arr['pages']) $arr['next'] = $arr['curr']+1;
		else $arr['next'] = $arr['pages'];
	}
	return $arr;
}


/*
 * get2path - convert $_GET array to frameworks query string
 * 
 * @return string
 */

function get2path($arr){
	$s = '';
	if(empty($arr)) return $s;
	foreach($arr as $k=>$v) 
		$s .= '-'.$k.'/'.$v.'/';
	return $s;
}
