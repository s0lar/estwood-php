<?php
/*
 *
 * image method ( crop / resize / no_change )
 *
 * */

return array(

    //    MYSQL config
    'dsn' => 'mysqli://glukhanko_estwud:mrYd1fja@localhost:3306/glukhanko_estwud?encoding=utf8&table_prefix=estwood__',
    'dsn_local' => 'mysqli://root:123@127.0.0.1:3306/estwood?encoding=utf8&table_prefix=estwood__',

    //     Defaults
    'app' => array(
        'storage_dir' => 'f/',
    ),

    //    Files config
    'file' => array(
        '0' => array( //    Default image thumb
            'allow' => '', //     Files allowed to upload
            'disallow' => '', //     Files disallowed to upload (if "allow" not empty than "disallow" skip )
            'count' => 0, //     Count of files you can upload. If set 0 than no limits
            'prefix_dir' => 'default_files', //
        ),

        'pdf' => array( //    Default image thumb
            'allow' => '', //     Files allowed to upload
            'disallow' => '', //     Files disallowed to upload (if "allow" not empty than "disallow" skip )
            'count' => 0, //     Count of files you can upload. If set 0 than no limits
            'prefix_dir' => 'pdf', //
        ),

    ),

    //    Images config
    'image' => array(
        '0' => array( //    Default image thumb
            'allow' => 'jpg,gif,png', //     Files allowed to upload
            'disallow' => '', //     Files disallowed to upload (if "allow" not empty than "disallow" skip )
            'prefix_dir' => 'default', //
            'count' => 0, //     Count of files you can upload. If set 0 than no limits
            'thubms' => array(
                'a' => array(
                    'width' => 0,
                ),
            ),
        ),

        'blog' => array( //    Default image thumb
            'allow' => 'jpg,gif,png', //     Files allowed to upload
            'disallow' => '', //     Files disallowed to upload (if "allow" not empty than "disallow" skip )
            'prefix_dir' => 'catalog', //
            'count' => 0, //     Count of files you can upload. If set 0 than no limits
            'thubms' => array(
                'a' => array(
                    'width' => 520,
                    'height' => 340,
                    'method' => 'crop',
                ),
            ),
        ),

        'service' => array( //    Default image thumb
            'allow' => 'jpg,gif,png', //     Files allowed to upload
            'disallow' => '', //     Files disallowed to upload (if "allow" not empty than "disallow" skip )
            'prefix_dir' => 'service', //
            'count' => 0, //     Count of files you can upload. If set 0 than no limits
            'thubms' => array(
                'a' => array(
                    'width' => 200,
                    'height' => 200,
                    'method' => 'resize',
                ),
                'b' => array(
                    'width' => 1000,
                    'height' => 1000,
                    'method' => 'no_change',
                ),
            ),
        ),

        'prod_noresize' => array( //    Default image thumb
            'allow' => 'jpg,gif,png', //     Files allowed to upload
            'disallow' => '', //     Files disallowed to upload (if "allow" not empty than "disallow" skip )
            'prefix_dir' => 'catalog', //
            'count' => 0, //     Count of files you can upload. If set 0 than no limits
            'thubms' => array(
                'a' => array(
                    'width' => 200,
                    'height' => 200,
                    'method' => 'resize',
                ),
                'b' => array(
                    'width' => 1000,
                    'height' => 1000,
                    'method' => 'no_change',
                ),
            ),
        ),

        'prod_5' => array( //    Default image thumb
            'allow' => 'jpg,gif,png', //     Files allowed to upload
            'disallow' => '', //     Files disallowed to upload (if "allow" not empty than "disallow" skip )
            'prefix_dir' => 'catalog', //
            'count' => 0, //     Count of files you can upload. If set 0 than no limits
            'thubms' => array(
                'a' => array(
                    'width' => 200,
                    'height' => 200,
                    'method' => 'resize',
                ),
                'b' => array(
                    'width' => 1000,
                    'height' => 1000,
                    'method' => 'no_change',
                ),
            ),
        ),

        'source_bg' => array( //    Default image thumb
            'allow' => 'jpg,gif,png', //     Files allowed to upload
            'disallow' => '', //     Files disallowed to upload (if "allow" not empty than "disallow" skip )
            'prefix_dir' => 'source', //
            'count' => 0, //     Count of files you can upload. If set 0 than no limits
            'thubms' => array(
                'a' => array(
                    'width' => 200,
                    'height' => 200,
                    'method' => 'resize',
                ),
                'b' => array(
                    'width' => 1000,
                    'height' => 1000,
                    'method' => 'no_change',
                ),
            ),
        ),

    ),
);
