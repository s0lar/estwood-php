(function ($) {
    $.fn.fm_loader = function (options) {

		/*
		var settings = {
			//act 	: 'add',
			el 		: '',
			key 	: '',
			type 	: 'file',
		};

		if (options) {
			$.extend(settings, options);
		}
		*/
        var sett = $.extend({
            el: '',
            key: '',
            type: 'file',
            hold: '',
        }, options);

        var Ga = {};

        //	Get JSON data about files from input
        Ga.getFM_DATA = function (holder) {
            //	Get existing information about files
            var obj = $(holder).val();

            // If empty than create new object
            if (obj == '')
                obj = { 'type': '', 'key': '', 'files': [] };

            //	Else parse JSON
            else
                obj = $.evalJSON(obj);

            return obj;
        }

        //	Save JSON data about files in input
        Ga.saveFM_DATA = function (holder, input_object) {
            $(holder).val($.toJSON(input_object));
        }

        //	Load gallery
        Ga.show = function (el, $gal) {
            //__(el, $gal, sett);
            var $elem = $('#' + el);
            //__($elem);
            $elem.empty();

            if ($gal.type == "image") {
                for (k in $gal.files) {
                    if (typeof $gal.files[k].fs === 'object') {
                        for (first in $gal.files[k].fs) break;
                        $elem.append(
                            "<li class='ga-item' rel='" + k + "'>" + "<div class='ga-col'><img class='ga-img' src='" + $gal.files[k].fs[first] + "' alt='' /></div>" + "<label class='ga-col ga-name'><input type='text' name='file-name' value='' /> - имя файла</label>" + "<label class='ga-col ga-title'><input type='text' name='file-title' value='' /> - подпись</label>" + "<label class='ga-col ga-del'><a href='/fm/?act=del&type=image&id=" + $gal.files[k].id + "' class='ga-del icon-remove'></a></label>" + "</li>"
                        );
                    }
                }
            }
            else {
                for (k in $gal.files) {
                    if (typeof $gal.files[k].fs === 'object') {
                        for (first in $gal.files[k].fs) break;
                        //__($gal.files[k].fs[first]);
                        $elem.append(
                            "<li class='ga-item' rel='" + k + "' style='width:80%'>" + "<div class='ga-col'><a target='_blank' href='" + $gal.files[k].fs[first] + "'>" + $gal.files[k].n + "</a></div>" + "<label class='ga-col ga-name'><input type='text' name='file-name' value='' /> - имя файла</label>" + "<label class='ga-col ga-title'><input type='text' name='file-title' value='' /> - подпись</label>" + "<label class='ga-col ga-del'><a href='/fm/?act=del&type=image&id=" + $gal.files[k].id + "' class='ga-del icon-remove'></a></label>" + "</li>"
                        );
                    }
                }
            }

            //deletemetod
            $elem.find(".ga-del").live('click', function () {
                $.ajax({
                    type: 'get',
                    dataType: 'json',
                    url: $(this).attr('href'),
                    success: function (data) {
                        if (data) {
                            if (data.success) {
                                //	Get JSON from field
                                input_object = Ga.getFM_DATA(sett.hold);
                                //	Delete record from JSON
                                for (i in input_object.files) {
                                    if (input_object.files[i].id == data.id) {
                                        input_object.files.splice(i, 1);
                                    }
                                }
                                //	Save JSON in field
                                Ga.saveFM_DATA(sett.hold, input_object);
                            }
                            else if (data.error) { alert("Error: " + data.error); }
                            else { __(data); }
                        }
                    }
                });
                $(this).parents('.ga-item').remove();
                $elem.find('.ga-item').each(function (k, v) { $(this).attr({ 'rel': k }); });

                return false;
            });

            Ga.sort();
        }

        Ga.sort = function () {
            ///sort
            var $gallery = $('#' + sett.el + '-ga');
            $gallery.dragsort("destroy");
            $gallery.dragsort({ dragSelector: "li", dragEnd: saveOrder, placeHolderTemplate: "<li class='ga-item-place-holder'></li>" });

            function saveOrder() {
                var data = $gallery.find('.ga-item').map(function () { return $(this).attr('rel') }).get();

                //	Get JSON from field
                input_object = Ga.getFM_DATA(sett.hold);

                //	Set new values
                var tmp = [];
                for (i in data) {
                    tmp.push(input_object.files[data[i]]);
                }
                input_object.files = tmp;

                //	Save JSON in field
                Ga.saveFM_DATA(sett.hold, input_object);

                //	Reset 'rel' attribute
                $gallery.find('.ga-item').each(function (k, v) { $(this).attr({ 'rel': k }); });
            };
        }



        //	main
        var init = function () {

            input_object = Ga.getFM_DATA(sett.hold);
            var fm_key = input_object.key;
            if (fm_key == '')
                fm_key = md5(new Date() + Math.random() + 'solt');
            var fm_type = sett.type;
            var fm_mask = sett.mask;

            //	Load gallery
            Ga.show(sett.el + '-ga', input_object);

            new qq.FileUploader({
                element: $('#' + sett.el + '-fine-uploader')[0],
                action: '/fm/?act=add&type=' + fm_type + '&mask=' + fm_mask + '&key=' + fm_key,
                debug: true,
                uploadButtonText: 'Выберите файл(ы)',

                onComplete: function (id, fileName, responseJSON) {
                    if (responseJSON.success) {
                        //	Get JSON from field
                        input_object = Ga.getFM_DATA(sett.hold);

                        //	Set values
                        input_object.key = responseJSON.key;
                        input_object.type = responseJSON.type;
                        input_object.files.push(responseJSON.files);

                        //	Save JSON in field
                        $(sett.hold).html($.toJSON(input_object));

                        //	Load gallery
                        Ga.show(sett.el + '-ga', input_object);

                        //	Save JSON in field
                        Ga.saveFM_DATA(sett.hold, input_object);
                    }
                    else {
                        alert(responseJSON.error);
                    }
                }
            });

            //	FIX
			/*
			$('a[href="#tabpdf"]').livequery('click', function () {
				initializeUploader($('#pdfuploader')[0]);
			});
			$('a[href="#tabphotos"]').livequery('click', function () {
				initializeUploader($('#photouploader')[0]);
			});
			*/
            //__("debug - " , abc);
        }

        return this.each(function () {
			/*
			sett.el 	= $(this).attr('fm:el');
			sett.mask 	= $(this).attr('fm:mask');
			sett.type 	= $(this).attr('fm:type');
			sett.hold 	= '#'+sett.el+'_input';

			__(sett);
			*/
            init();
        });
    };
})(jQuery);


function init_qq(id_name) {
    //__('init_qq');
    $e = $(id_name);
    $e.css({ border: '1px solid red' });

    //__( $e.attr('fm:el') );

    $e.fm_loader({ el: $e.attr('fm:el'), mask: $e.attr('fm:mask'), type: $e.attr('fm:type'), hold: '#' + $e.attr('fm:el') + '_input' });
    //$e.fm_loader();

}
