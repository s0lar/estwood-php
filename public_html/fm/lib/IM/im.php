<?php
/*
 * 
 */

define('IM_PATH', realpath(dirname(__FILE__)));
require(IM_PATH . "/ci_image_lib.class.php");

define("_DR",$_SERVER["DOCUMENT_ROOT"]);
define("_FILES_PERM",0777);
//
function im_crop($file, $w, $h, $is_thumb = true, $thumb = "crop")
{
	$i = getimagesize($file);
	if( what_is_longer($i[0], $i[1], $w, $h) )
	{
		$file = im_resize($file, $w, 5000, $is_thumb, $thumb);
		//chmod($file,0666);
		$ni = getimagesize($file);
		if( @$ni[1]-$h > 0 )
		{
			$_c['y_axis'] = (@$ni[1]-$h)/2;
		}
	}
	else 
	{
		$file = im_resize($file, 5000, $h, $is_thumb, $thumb);
		//chmod($file,0666);
		$ni = getimagesize($file);
		if( @$ni[0]-$w > 0 )
		{
			$_c['x_axis'] = ($ni[0]-$w)/2;
		}
	}
	$_c['image_library'] = 'gd2';
	$_c['source_image'] = $file;
	$_c['maintain_ratio'] = false;
	$_c['width'] = $w;
	$_c['height'] = $h;
	
	$im = new CI_Image_lib($_c);
	if($im->crop())
		return $_c['source_image'];
	else 	
		return false;
}
//
function im_resize($file, $w, $h, $is_thumb = true, $thumb = "thumb", $maintain_ratio = true){
	$_c['image_library'] 	= 'gd2';
	if(@file_exists($file))
	{
		$_c['source_image'] = $file;
	}
	elseif(@file_exists(_DR.$file))
	{
		$_c['source_image'] 	= _DR.$file;
	}
	$_c['create_thumb'] 	= $is_thumb; // ставим флаг создания эскиза
	$_c['thumb_marker'] 	= "_".$thumb; // ставим флаг создания эскиза
	$_c['maintain_ratio'] 	= $maintain_ratio; // сохранять пропорции
	$_c['width'] 			= $w; 	// и задаем размеры
	$_c['height'] 			= $h;
	$im = new CI_Image_lib($_c);
	if($im->resize()) {
		if($is_thumb){
			$arr = explode(".", $_c['source_image']);
			$arr[count($arr)-2] .= "_".$thumb;
			return implode(".",$arr);
		}
		else
			return $_c['source_image'];
		
	} else
		return false;
}
//
function im_watermark($source, $water_mark){
	$_c['source_image'] 	= $source;
	$_c['wm_type'] 			= 'overlay';
	$_c['wm_vrt_alignment'] = 'm';
	$_c['wm_hor_alignment'] = 'c';
	$_c['wm_hor_offset'] 	= 0;
	$_c['wm_vrt_offset'] 	= 0;
	$_c['wm_overlay_path'] 	= _DR.$water_mark;
	$_c['wm_opacity'] 		= 50;
	$_c['wm_x_transp'] 		= 1;
	$_c['wm_y_transp'] 		= 1;
	$im = new CI_Image_lib($_c);
	return $im->watermark();
}
//


/*
 * Вычисляем что больше ширина или высота. Необходимо для функции im_crop 
 */
function what_is_longer( $w_src, $h_src, $w, $h ){
	$xwidth = ($w_src * $h) / $h_src;
	if( $xwidth < $w)
		return true;
	else
		return false;
}

?>