$(document).ready(function(){

	/*
	 * Header
	 */

	$(function(){
		var $header = $('.header'),
			$submenu = $('.header-submenu'),
			$body = $('body');

		$(document)
			.on('click', function(event){
			    $header.removeClass('is-open');
			    $submenu.removeClass('is-open');
			    $body.removeClass('menu-is-open');
			})
			.on('click', '.header-menu, .header-submenu', function(event){
				event.stopPropagation();
			})
			.on('click', '.js-submenuLink', function(event){
				var parent = $(this).parent();
				var navlist = parent.find('.header-nav__list');

				if($(window).width() >= 768){
					$('' + $(this).data('submenu') + '').toggleClass('is-open');
					return false;
				}
				else {
					if(window.orientation == 0){
						navlist.slideToggle(200);
						return false;
					}
				}
			})
			.on('click', '.js-catalogBurger', function(event){
				event.stopPropagation();
				$('' + $(this).data('submenu') + '').toggleClass('is-open');
			})
			.on('click', '.js-burgerMenu', function(event){
				event.stopPropagation();
				$('.filter').hasClass('is-open') && $('.js-filterClose').trigger('click');
				$header.toggleClass('is-open');
				$submenu.removeClass('is-open');
				$body.toggleClass('menu-is-open');

			});
	});

	$(function(){
		/*
		$(window).on('load', function(){
			preloaderHide();
			mainAnimationInit();
		});
		*/

		setTimeout(function(){
			preloaderHide();
			mainAnimationInit();
		}, 3000);

		function preloaderHide(){
			$('.preloader').addClass('is-hide');

			setTimeout(function(){
				$('.preloader').remove();
			}, 700);
		}

		function mainAnimationInit(){
			$('.animate').removeClass('animate');

			if($('body').hasClass('main-p')){
				var video = $('#video-main'),
					mainBack = $('.main__back');

				if(video.length){
					video[0].play();
				}

				var timeoutLogo = 0;

				//if($(window).width() >= 768 && $(window).width() <= 1024)
              	if($(window).width() >= 768)
					timeoutLogo = 1000;

				//if($(window).width() >= 1025)
					//timeoutLogo = 1000;

				setTimeout(function(){
					animateLogo();
				}, timeoutLogo);

				video[0].ontimeupdate = function(){
					if(video[0].currentTime > (video[0].duration - 2)){
						mainBack.addClass('is-show');
					}
				}
			}
			else {
				animateLogo();
			}
		}


		var logoShift = 0;
		var isMobile = false;

		function animateLogo() {
			logoShift = 0;
			isMobile = $(window).width() < 768;
			setTimeout(function () {
				logoAnimStep();
			}, 500)
		}

		function logoAnimStep() {
			var logo = $('.logo');

			logo.css({'opacity': '1'});

			setTimeout(function () {
				logoShift += 70;
				if (logoShift >= 1680) {
					return;
				}
				logo.css('background-position', '-' + logoShift + 'px 0');
				logoAnimStep();
			}, 45);
		}

		if($(window).width() >= 768){
			$(document).on('mouseenter', '.logo', function(){
				animateLogo();
			});
		}
	});

	/*
	 * Валидация формы
	 */

	$(function(){
		var form = $('.js-validate');

		form.each(function(){
			$(this).validate({
				rules: {
					EMAIL: {
						required: true,
						email: true
					}
				},
				errorPlacement: function (error, element) {},
				highlight: function(element) {
					$(element).parent().addClass('error');
				},
				unhighlight: function(element) {
					$(element).parent().removeClass('error');
				},
				submitHandler: function(form){
					//form.submit();
				}
			});
		});


		var phone = $('.js-phone'),
		email = $('.js-email');

		$(document).on('change', '.js-phone', function(){
			$(this).val() != '' ? email.removeAttr('required', 'required') : email.attr('required', 'required');
		});

		$(document).on('change', '.js-email', function(){
			$(this).val() != '' ? phone.removeAttr('required', 'required') : phone.attr('required', 'required');
		});
	});

	// Маска ввода телефона
	if($('.js-phone').length){
		$('.js-phone').mask('?+7 (999) 999-99-99');
	}

	if($(window).width() >= 768){
		var catalogSwiper = new Swiper('.js-catalogSwiper', {
			speed: 800,
			slidesPerView: 'auto',
			mousewheelControl: true,
			simulateTouch: false,
			nextButton: '.js-catalogSwiper .swiper-button--next',
			prevButton: '.js-catalogSwiper .swiper-button--prev',
			onSlideChangeStart: function(swiper){
				var slideCount = $('.js-catalogSwiper .swiper-slide').length - 1,
					activeSlideIndex = swiper.realIndex;

				if(activeSlideIndex == slideCount){
					$('body').addClass('is-last-slide');
				}
				else {
					$('body').removeClass('is-last-slide');
				}
			}
		});

		if($('.js-catalogSwiper').length > 0 && window.location.href.indexOf('?id=') >= 0){
			var id = window.location.href.split('?id=')[1];
			var product = $('#' + id + '');
			catalogSwiper.slideTo(product.parent().index());
		}

		$('.scroll-container')
			.on('mousemove', function(){
				catalogSwiper.disableMousewheelControl();
			})
			.on('mouseleave', function(){
				catalogSwiper.enableMousewheelControl();
			});
	}
	else {
		if($('.js-catalogSwiper').length > 0 && window.location.href.indexOf('?id=') >= 0){
			var id = window.location.href.split('?id=')[1];
			var product = $('#' + id + '');
			if(product.length){
				$('body, html').animate({
					scrollTop: product.offset().top - 50
				}, 600);
			}
		}
	}

	$(document).on('mouseenter', '.js-filterBurger', function(){
		$('.filter').addClass('is-open');
	});

	$(document).on('mouseleave', '.filter', function(){
		$('.filter').removeClass('is-open');
	});

	$(document).on('click', '.js-filterClose', function(){
		$('.filter').removeClass('is-open');
	});

	$(document).on('click', '.js-filterElemToggle', function(){
		$(this).toggleClass('is-open');
		$(this).next().slideToggle(200);
	});

	$(document).on('click', '[data-tab-nav]', function(){
		var _this = $(this);

		_this
			.addClass('is-active')
			.siblings()
			.removeClass('is-active');

		$('' + _this.attr('data-tab-nav') + '')
			.show()
			.siblings()
			.hide();

		$('[data-tab-material="' + _this.attr('data-tab-nav') + '"]')
			.addClass('is-show')
			.siblings()
			.removeClass('is-show');

	});


	var serviceSwiper = new Swiper('.js-serviceSwiper', {
		speed: 600,
		slidesPerView: 3,
		mousewheelControl: true,
		loop: true,
		pagination: '.js-serviceSwiper .swiper-pagination',
		nextButton: '.js-serviceSwiper .swiper-button--next',
		prevButton: '.js-serviceSwiper .swiper-button--prev',
		breakpoints: {
			1025: {
				slidesPerView: 1
			}
		}
	});

	if($(window).width() >= 768){
		var aboutSwiper = new Swiper('.js-aboutSwiper', {
			speed: 800,
			direction: 'vertical',
			slidesPerView: 1,
			mousewheelControl: true,
			noSwipingClass: 'swiper-no-swiping',
			pagination: '.js-aboutSwiper .swiper-pagination',
			paginationClickable: true,
			onSlideChangeStart: function(){
				var slideArrays = [1, 5, 9];
				var slideArrays2 = [7, 9];
				var activeSlide = $('.js-aboutSwiper .swiper-slide-active').index();
				var body = $('body');

				body.removeClass('nav-is-gray header-info-is-gray');

				$.each(slideArrays, function(i){
					if(slideArrays[i] == activeSlide){
						body.addClass('nav-is-gray');
					}
					if(slideArrays2[i] == activeSlide){
						body.addClass('header-info-is-gray');
					}
				});
			}
		});
	}

	/*
	 * Карта
	 */

	function initializeMap(){
		var WINDOW_WIDTH = $(window).width(),
			mapContainer = $('.js-map'),
			pin = mapContainer.data('pin'),
			loc = mapContainer.data('loc').split(', '),
			positionOffsetLeft = positionOffsetLeft(),
			positionOffsetTop = positionOffsetTop(),

			position = new google.maps.LatLng(parseFloat(loc[0]) + positionOffsetTop, loc[1] - positionOffsetLeft),
			mapOptions = {
		        center: position,
		        zoom: 17,
		        mapTypeId: google.maps.MapTypeId.ROADMAP,
		        mapTypeControlOptions: {

		        },
		        disableDefaultUI: true,
		        scrollwheel: false,
		        rotateControl: false,
		        styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"on"},{"saturation":-100},{"lightness":20}]},{"featureType":"road","elementType":"all","stylers":[{"visibility":"on"},{"saturation":-100},{"lightness":40}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"on"},{"saturation":-10},{"lightness":30}]},{"featureType":"landscape.man_made","elementType":"all","stylers":[{"visibility":"simplified"},{"saturation":-60},{"lightness":10}]},{"featureType":"landscape.natural","elementType":"all","stylers":[{"visibility":"simplified"},{"saturation":-60},{"lightness":60}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"},{"saturation":-100},{"lightness":60}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"},{"saturation":-100},{"lightness":60}]}]
	    	};

	    function positionOffsetLeft(){
	    	var num = 0;

	    	if($(window).width() >= 1025)
	    		num = -0.0025;

	    	if($(window).width() >= 768 && $(window).width() <= 1024 && $(window).height() <= 1023)
				num = -0.0028;

	    	return num;
	    }

	    function positionOffsetTop(){
	    	var num = 0;

	    	if($(window).width() >= 768 && $(window).width() <= 1024 && $(window).height() >= 1024)
	    		num = 0.0018;

	    	return num;
	    }

	    // console.log(loc[1] - positionOffsetLeft)

	    var map = new google.maps.Map(mapContainer.get(0), mapOptions);
	    map.setTilt(45);

		position = new google.maps.LatLng(loc[0], loc[1]);

	    var marker = new google.maps.Marker({
	        position: position,
	        map: map,
	        title: '',
	        animation: google.maps.Animation.DROP,
	        icon: {
	            url: pin,
	            scaledSize: new google.maps.Size(64, 53)
	        }
	    });

	    var moscowPos = new google.maps.LatLng(55.714282, 37.589132800000016);
	    var markerMoscow = new google.maps.Marker({
	        position: moscowPos,
	        map: map,
	        title: '',
	        animation: google.maps.Animation.DROP,
	        icon: {
	            url: pin,
	            scaledSize: new google.maps.Size(64, 53)
	        }
	    });

		map.setOptions({draggable: WINDOW_WIDTH > 767});

		$('[data-coords]').on('click', function(){
			var coords = $(this).data('coords').split(', ');
			var pos = new google.maps.LatLng(parseFloat(coords[0]) + positionOffsetTop, coords[1] - positionOffsetLeft);

			map.panTo(pos);
		});
	}

	if($('body').find('.js-map').length){
		google.maps.event.addDomListener(window, 'load', initializeMap);
	}

	/*
	 * Contact
	 */

	$(document).on('click', '.js-contactModalLink', function(){
		$('.js-contactModal').addClass('is-show');
	});

	$(document).on('click', '.js-contactModalClose', function(){
		$('.js-contactModal').removeClass('is-show');
	});

	$(document).on('click', '.js-feedbackModalClose', function(){
		$('.js-feedbackModal').removeClass('is-show');
	});

	$(document).on('click', '.js-modalLink', function(event){
		event.stopPropagation();
		var currentModal = $('' + $(this).data('modal') + '');
		currentModal.addClass('is-open');
		setTimeout(function(){
			currentModal.find('.material-grid__elem.is-active').trigger('click');
		}, 100);

	});

	$(document).on('click', '.js-modalClose', function(){
		$('.modal').removeClass('is-open');
	});

	$(document).on('click', function(event){
		if($('.modal__box').has(event.target).length === 0){
		    $('.modal').removeClass('is-open');
		}
	});

	$(document).on('click', '.material-grid__elem', function(){
		$(this).addClass('is-active').siblings().removeClass('is-active');
		$('.js-materialImage').attr('src', $(this).data('image'));
	});

	$(document).on('click', '.js-contactTabLink', function(event){
		event.preventDefault();

		var _this = $(this);
		var id = _this.attr('href');
		var content = $('' + id + '');

		_this
			.addClass('is-active')
			.siblings()
			.removeClass('is-active');

		content
			.show()
			.siblings()
			.hide();
	});

	/*
	 * Full page scrolling
	 */

	$(function(){
		if($(window).width() >= 768){

			var fullpageSwiper = new Swiper('.js-fullpageSwiper', {
				speed: 800,
				mousewheelControl: true,
				// noSwiping: false,
				simulateTouch: false,
				noSwipingClass: 'swiper-no-swiping',
				onSlideChangeStart: function(){
					var slideArrays = [1, 2, 4, 5];
					var activeSlide = $('.js-fullpageSwiper .swiper-slide-active').index();
					var body = $('body');

					body.removeClass('nav-is-gray');

					$.each(slideArrays, function(i){
						if(slideArrays[i] == activeSlide){
							body.addClass('nav-is-gray');
						}
					});

					$('[data-slide-index]').removeClass('is-active');
					$('[data-slide-index="'+ (activeSlide + 1) +'"]').addClass('is-active');
				},
				breakpoints: {
					6000: {
						noSwiping: true
					},
					1024: {
						noSwiping: false
					}
				}
			});

			$(document).on('click', '[data-slide-index]', function(){
				fullpageSwiper.slideTo($(this).data('slide-index') - 1);
			});
		}
		if($(window).width() <= 767){
			$(document).on('click', '.js-arrowDown', function(){
				$('body, html').animate({
					scrollTop: $(window).height() - 50
				}, 400);
			});
		}

		var rangeSlider = $('.js-rangeSlider'),
			rangeImage = $('.js-rangeImage');

		if(rangeSlider.length) {
			rangeSlider.ionRangeSlider({
				min: 0,
				max: 100,
				step: 0.1,
				from: 0,
				grid_margin: false,
				decorate_both: false,
				// onFinish: function(){
				// 	if($(window).width() >= 768)
				// 		fullpageSwiper.unlockSwipes();
				// },
				onChange: function(arguments){
					var left = arguments.from;

					// if($(window).width() >= 768)
					// 	fullpageSwiper.lockSwipes();

					rangeImage.css({
						'transform': 'translate3d('+ -left +'%, 0, 0)'
					});
				}
			});
		}

	});

	/*
	 * Scroll to content
	 */

	$(function(){
		$(document).on('click', '[data-scroll-link]', function(){
			var offsetMargin = $(window).width() <= 767 ? -50 : 0;

			$('body, html').animate({
				scrollTop: $('' + $(this).data('scroll-link') + '').offset().top + offsetMargin
			}, 600);
		});
	});



	/*
	 * Blog
	 */

	var blogSwiper = new Swiper('.js-blogSwiper', {
		speed: 600,
		slidesPerView: 3,
		mousewheelControl: true,
		nextButton: '.js-blogSwiper .swiper-button--next',
		prevButton: '.js-blogSwiper .swiper-button--prev',
		breakpoints: {
			1025: {
				slidesPerView: 1
			},
			767: {
				slidesPerView: 1,
				mousewheelControl: false
			}
		}
	});

	if($(window).width() >= 768){
		$(window).on('resize', function(){
			$('.scroll-container').height($(window).height() - 200);
		});

		$('.scroll-container').height($(window).height() - 200);

		$('.js-scroll').jScrollPane({
			autoReinitialise: true,
			verticalDragMinHeight: 24,
			verticalDragMaxHeight: 24,
			horizontalDragMinWidth: 28,
			horizontalDragMaxWidth: 28,
			showArrows: true,
			horizontalGutter: 30,
			verticalGutter: 30
		});
	}

	$(function(){
		$(window).on('scroll', function(){
			var scrolled = $(window).scrollTop();
			var windowHeight = $(window).height();

			if(scrolled < windowHeight){
				$('.js-buttonScrollUp').removeClass('is-show');
			}
			else {
				$('.js-buttonScrollUp').addClass('is-show');
			}
		});

		$('.js-buttonScrollUp').on('click', function(){
			$('body, html').animate({
				scrollTop: 0
			}, 500);
		});
	});
});
