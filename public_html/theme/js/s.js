
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 	CALLBACKS
var callbacks = {

	/* Review forms callbacks */
	feedback_success: 	function(data, $form) {
		$form.hide();
		var $p = $form.parents('.screen-form');

		$p.find('.screen-form__title').empty().append('Заявка отправлена');
		$p.find('.medium__title').empty().append('Заявка отправлена');
		$p.find('.medium__title').empty().append('Заявка отправлена');
		$p.find('.screen-form__text').empty().append('<p>Спасибо за ваше обращение! Наши менеджеры свяжутся с вами в ближайшее время, чтобы уточнить все детали.</p><p>&nbsp;</p>');

		var $p = $form.parents('.form-section');
		$p.find('.screen-form__title').empty().append('Заявка отправлена');
		$p.find('.screen-form__text').empty().append('<p>Спасибо за ваше обращение! Наши менеджеры свяжутся с вами в ближайшее время, чтобы уточнить все детали.</p><p>&nbsp;</p>');

	},
	feedback_before: 	function($form) {
	},
	feedback_error: 		function(data, $form) {
		console.log( data.error );
	},
}



//
$(document).ready(function(){

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Forms validate
	//$('form.validate').validate();

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Links ajax send
		$("a.ajax").click(function(){
			var callback_prefix = $(this).attr('rel').replace(/-/g, '_');
			$.ajax({
				type: 'get',
				dataType: 'json',
				url: $(this).attr('href')+'.json',
				beforeSend: function(){
					callbacks[callback_prefix + '_before'] ? callbacks[callback_prefix + '_before']() : callbacks['default_before']();
				},
				success: function(data){
					if (data) {
						if (!data.success) callbacks[callback_prefix + '_error'] ? callbacks[callback_prefix + '_error'](data, $(this)) : callbacks['default_error'](data, $(this));
						else callbacks[callback_prefix + '_success'] ? callbacks[callback_prefix + '_success'](data, $(this)) : callbacks['default_success'](data, $(this));
					}
				}
			});
			return false;
		});

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Forms ajax send
		$("form.ajax").submit(function(event){
			event.preventDefault();
			//	Validation
			// if (!$(this).hasClass('xv-valid'))
			// 	return false;

			$form = $(this);
			var callback_prefix = $form.attr('rel').replace(/-/g, '_');

			console.log('callback_prefix', callback_prefix);


			$.ajax({
				type: $form.attr('method'),
				dataType: 'json',
				url: $form.attr('action'),
				data: $form.serialize(),
				beforeSend: function(){
					callbacks[callback_prefix + '_before'] ? callbacks[callback_prefix + '_before']($form) : callbacks['default_before']($form);
				},
				success: function(data){
					if (data) {
						if (!data.success)
							callbacks[callback_prefix + '_error'] ? callbacks[callback_prefix + '_error'](data, $form) : callbacks['default_error'](data, $form);
						else
							callbacks[callback_prefix + '_success'] ? callbacks[callback_prefix + '_success'](data, $form) : callbacks['default_success'](data, $form);
					}
				}
			});

			return false;
		});

});